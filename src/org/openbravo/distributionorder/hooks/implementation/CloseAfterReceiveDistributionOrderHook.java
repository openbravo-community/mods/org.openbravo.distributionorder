/*
 ************************************************************************************
 * Copyright (C) 2022 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package org.openbravo.distributionorder.hooks.implementation;

import org.hibernate.ScrollableResults;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.DistributionOrderLine;
import org.openbravo.distributionorder.erpCommon.utility.DistributioOrderUtils;
import org.openbravo.distributionorder.erpCommon.utility.ProcessDistributionOrderUtil;
import org.openbravo.distributionorder.hooks.AfterUpdateDistributionOrderFromGoodsMovementHook;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;

/**
 * Triggers the event of distribution order's closing action after processed. The closing action is
 * performed when:
 * <ul>
 * <li>The preference Close DO Receipt automatically after fully reception is set to Y
 * <li>Lines have the quantity received equals or greater than the quantity confirmed
 * <li>Or the lines have the quantity received equals or greater than the quantity issued and the
 * distribution order referenced is closed
 * </ul>
 */
public class CloseAfterReceiveDistributionOrderHook
    extends AfterUpdateDistributionOrderFromGoodsMovementHook {

  @Override
  public int getOrder() {
    return Integer.MAX_VALUE - 10;
  }

  @Override
  public boolean isValid(InternalMovement movement) {
    return isAutomaticCloseConfigured();
  }

  @Override
  public void run(InternalMovement movement) {
    DistributionOrder distributionOrder = movement.getObdoDistorder();
    if (distributionOrder != null && !distributionOrder.isSalesTransaction()) {
      boolean closeDO = analyzeDONeedsToBeClosed(distributionOrder);
      if (closeDO) {
        ProcessDistributionOrderUtil.processDistributionOrder(distributionOrder);
      }
    }
  }

  private static boolean analyzeDONeedsToBeClosed(DistributionOrder distributionOrder) {
    try (final ScrollableResults linesDO = DistributioOrderUtils
        .getDistOrderLinesScrollableResult(distributionOrder)) {
      while (linesDO.next()) {
        final DistributionOrderLine distOrdline = (DistributionOrderLine) linesDO.get(0);
        if (!canBeClosed(distOrdline)) {
          return false;
        }
      }
    }
    return true;
  }

  private static boolean canBeClosed(DistributionOrderLine line) {
    return (line.getQtyReceived().compareTo(line.getQtyConfirmed()) >= 0)
        || (line.getQtyReceived().compareTo(line.getQtyIssued()) >= 0
            && ProcessDistributionOrderUtil.DOCSTATUS_CLOSE
                .equals(line.getDistributionOrder().getReferencedDistorder().getDocumentStatus()));
  }

  private static boolean isAutomaticCloseConfigured() {
    return ProcessDistributionOrderUtil.getPreferenceValue("OBDO_CloseAfterReceipt");
  }

}
