/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.hooks.implementation;

import org.hibernate.ScrollableResults;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.DistributionOrderLine;
import org.openbravo.distributionorder.hooks.AfterDistributionOrderProcessedHook;
import org.openbravo.materialmgmt.ReservationUtils;
import org.openbravo.model.materialmgmt.onhandquantity.Reservation;

/**
 * When closing a DO, it automatically close any open linked reservation
 */
public class AfterDOProcessedHook extends AfterDistributionOrderProcessedHook {
  public static final int ORDER = -100;

  @Override
  public int getOrder() {
    return ORDER;
  }

  @Override
  public boolean isValid(final DistributionOrder distributionOrder, final String docAction) {
    // To be run only when closing a DO
    return isBeingClosed(distributionOrder, docAction);
  }

  @Override
  public void run(final DistributionOrder distributionOrder, final String docAction) {
    if (releaseOpenReservations(distributionOrder) > 0) {
      OBDal.getInstance().flush(); // Sync db for further hooks
    }
  }

  private int releaseOpenReservations(final DistributionOrder distributionOrder) {
    int released = 0;
    try (final ScrollableResults distributionOrderLineScroll = getDistOrderLinesScrollableResult(
        distributionOrder)) {
      int i = 0;
      while (distributionOrderLineScroll.next()) {
        final DistributionOrderLine distributionOrderLine = (DistributionOrderLine) distributionOrderLineScroll
            .get(0);
        for (Reservation reservation : distributionOrderLine
            .getMaterialMgmtReservationEMObdoDistorderlineIDList()) {
          if (!reservation.getRESStatus().equals(DOCSTATUS_CLOSE)) {
            ReservationUtils.processReserve(reservation, DOCSTATUS_CLOSE);
            released++;
          }
        }
        if ((++i % 100) == 0) {
          OBDal.getInstance().getSession().clear();
        }
      }
    }
    return released;
  }

}
