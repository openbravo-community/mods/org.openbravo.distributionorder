/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2022 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.hooks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Executes the {@link AfterUpdateDistributionOrderFromGoodsMovementHook} just after a goods
 * movement has updated the distribution order's received/issued quantities
 * 
 */
@Dependent
public class AfterUpdateDistributionOrderFromGoodsMovementHookRunner {

  private static final Logger log4j = LoggerFactory
      .getLogger(AfterUpdateDistributionOrderFromGoodsMovementHookRunner.class);

  @Inject
  @Any
  public Instance<AfterUpdateDistributionOrderFromGoodsMovementHook> afterUpdateDOFromGMHook;

  /**
   * Execute hooks after a goods movement has updated the distribution order's received/issued
   * quantities
   */
  public void run(final InternalMovement movement) {
    log4j.debug("Starting execution of AfterUpdateDistributionOrderFromGoodsMovementHook hooks");

    final List<AfterUpdateDistributionOrderFromGoodsMovementHook> hooks = new ArrayList<>();
    for (Iterator<? extends Object> hookIterator = afterUpdateDOFromGMHook.iterator(); hookIterator
        .hasNext();) {
      AfterUpdateDistributionOrderFromGoodsMovementHook hook = (AfterUpdateDistributionOrderFromGoodsMovementHook) hookIterator
          .next();
      if (hook.isValid(movement)) {
        hooks.add(hook);
      }
    }

    Collections.sort(hooks, new AfterUpdateDistributionOrderFromGoodsMovementHookComparator());
    for (final AfterUpdateDistributionOrderFromGoodsMovementHook hook : hooks) {
      log4j.debug("Running AfterUpdateDistributionOrderFromGoodsMovementHook: {}", hook);
      hook.run(movement);
    }

    log4j.debug("Finishing execution of AfterDistributionOrderProcessedHook hooks");
  }

  private static class AfterUpdateDistributionOrderFromGoodsMovementHookComparator
      implements Comparator<AfterUpdateDistributionOrderFromGoodsMovementHook> {
    @Override
    public int compare(AfterUpdateDistributionOrderFromGoodsMovementHook a,
        AfterUpdateDistributionOrderFromGoodsMovementHook b) {
      return a.getOrder() < b.getOrder() ? -1 : a.getOrder() == b.getOrder() ? 0 : 1;
    }
  }
}
