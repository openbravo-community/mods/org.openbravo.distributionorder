/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2022 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.hooks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.openbravo.distributionorder.DistributionOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Executes the {@link BeforeDistributionOrderProcessedHook} just before processing a Distribution
 * Order
 *
 */

@Dependent
public class BeforeDistributionOrderProcessedHookRunner {

  private static final Logger log4j = LoggerFactory
      .getLogger(BeforeDistributionOrderProcessedHookRunner.class);

  @Inject
  @Any
  public Instance<BeforeDistributionOrderProcessedHook> beforeDistributionOrderProcessedHook;

  /**
   * Execute hooks before a Distribution Order has been processed
   */

  public void run(final DistributionOrder distributionOrder, String docAction) {
    log4j.debug("Starting execution of BeforeDistributionOrderProcessedHook hooks");

    final List<BeforeDistributionOrderProcessedHook> hooks = new ArrayList<>();
    for (Iterator<? extends Object> hookIterator = beforeDistributionOrderProcessedHook
        .iterator(); hookIterator.hasNext();) {
      BeforeDistributionOrderProcessedHook hook = (BeforeDistributionOrderProcessedHook) hookIterator
          .next();
      if (hook.isValid(distributionOrder, docAction)) {
        hooks.add(hook);
      }
    }

    Collections.sort(hooks, new BeforeDistributionOrderProcessedHookComparator());
    for (final BeforeDistributionOrderProcessedHook hook : hooks) {
      log4j.debug("Running BeforeDistributionOrderProcessedHook: {}", hook);
      hook.run(distributionOrder, docAction);
    }

    log4j.debug("Finishing execution of BeforeDistributionOrderProcessedHook hooks");
  }

  private static class BeforeDistributionOrderProcessedHookComparator
      implements Comparator<BeforeDistributionOrderProcessedHook> {
    @Override
    public int compare(BeforeDistributionOrderProcessedHook a,
        BeforeDistributionOrderProcessedHook b) {
      return a.getOrder() < b.getOrder() ? -1 : a.getOrder() == b.getOrder() ? 0 : 1;
    }
  }
}
