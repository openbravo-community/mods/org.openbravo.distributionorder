/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2022 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.hooks;

import org.openbravo.model.materialmgmt.transaction.InternalMovement;

/**
 * Abstract class to be implemented by hooks to be executed just after a goods movement has updated
 * the distribution order's received/issued quantities
 *
 */
public abstract class AfterUpdateDistributionOrderFromGoodsMovementHook {

  /**
   * Returns the order when the concrete hook will be implemented. A positive value will execute the
   * hook after the core's logic
   */
  public abstract int getOrder();

  /**
   * Returns true if this hooks should be executed for the Goods Movement
   * 
   * @param movement
   *          The movement that updates the issued/received quantities.
   */
  public abstract boolean isValid(final InternalMovement movement);

  /**
   * This method implements the logic to be run when a Distribution Order has been updated through a
   * goods movement
   * 
   * @param movement
   *          The movement that updates the issued/received quantities. The goods movement as
   *          parameter is linked always to a distribution order
   */
  public abstract void run(final InternalMovement movement);

}
