/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.hooks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.openbravo.distributionorder.DistributionOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Executes the {@link AfterDistributionOrderProcessedHook} just after processing a Distribution
 * Order
 * 
 */

@Dependent
public class AfterDistributionOrderProcessedHookRunner {

  private static final Logger log4j = LoggerFactory
      .getLogger(AfterDistributionOrderProcessedHookRunner.class);

  @Inject
  @Any
  public Instance<AfterDistributionOrderProcessedHook> afterDistributionOrderProcessedHook;

  /**
   * Execute hooks after a Distribution Order has been processed
   */

  public void run(final DistributionOrder distributionOrder, String docAction) {
    log4j.debug("Starting execution of AfterDistributionOrderProcessedHook hooks");

    final List<AfterDistributionOrderProcessedHook> hooks = new ArrayList<>();
    for (Iterator<? extends Object> hookIterator = afterDistributionOrderProcessedHook
        .iterator(); hookIterator.hasNext();) {
      AfterDistributionOrderProcessedHook hook = (AfterDistributionOrderProcessedHook) hookIterator
          .next();
      if (hook.isValid(distributionOrder, docAction)) {
        hooks.add(hook);
      }
    }

    Collections.sort(hooks, new AfterDistributionOrderProcessedHookComparator());
    for (final AfterDistributionOrderProcessedHook hook : hooks) {
      log4j.debug("Running AfterDistributionOrderProcessedHook: {}", hook);
      hook.run(distributionOrder, docAction);
    }

    log4j.debug("Finishing execution of AfterDistributionOrderProcessedHook hooks");
  }

  private static class AfterDistributionOrderProcessedHookComparator
      implements Comparator<AfterDistributionOrderProcessedHook> {
    @Override
    public int compare(AfterDistributionOrderProcessedHook a,
        AfterDistributionOrderProcessedHook b) {
      return a.getOrder() < b.getOrder() ? -1 : a.getOrder() == b.getOrder() ? 0 : 1;
    }
  }
}
