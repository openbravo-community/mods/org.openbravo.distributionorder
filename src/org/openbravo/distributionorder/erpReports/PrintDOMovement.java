/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.erpReports;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

import org.apache.log4j.Logger;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.client.application.report.ReportingUtils;

public class PrintDOMovement extends HttpSecureAppServlet {

  private static final long serialVersionUID = 1L;

  private static final Logger log = Logger.getLogger(PrintDOMovement.class);

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {

    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {

      String strmDistOrderId = vars.getSessionValue("PRINTDISTRIBUTIONORDERMOVEMENT.INPMMOVEMENTID")
          .replace("('", "")
          .replace("')", "");

      printPagePartePDF(response, vars, strmDistOrderId);
    } else
      pageError(response);
  }

  private void printPagePartePDF(HttpServletResponse response, VariablesSecureApp vars,
      String strmDistOrderId) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: pdf");
    String strBaseDesign = getBaseDesignPath(vars.getLanguage());
    HashMap<String, Object> parameters = new HashMap<String, Object>();
    JasperReport jasperReportLines;
    try {
      jasperReportLines = ReportingUtils.compileReport(
          strBaseDesign + "/org/openbravo/distributionorder/erpReports/OBDO_Movement_Lines.jrxml");
    } catch (JRException e) {
      log.error("Error generating report: " + e.getMessage(), e);
      throw new ServletException(e.getMessage());
    }

    parameters.put("SR_LINES", jasperReportLines);
    parameters.put("DOCUMENT_ID", strmDistOrderId);

    renderJR(vars, response,
        strBaseDesign + "/org/openbravo/distributionorder/erpReports/OBDO_Movement.jrxml", "pdf",
        parameters, null, null);
  }

  public String getServletInfo() {
    return "Servlet that presents the PrintOptions seeker";
  } // End of getServletInfo() method

}
