/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2022 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */
package org.openbravo.distributionorder.erpReports;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openbravo.client.application.report.ReportingUtils;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.erpCommon.utility.reporting.TemplateData;
import org.openbravo.service.db.DalConnectionProvider;

/**
 * Provides the report template information for a distribution order
 */
public class DistributionOrderReportTemplate {
  private static final Logger log = LogManager.getLogger();
  private static final String DEFAULT_TEMPLATE_FILE = "@basedesign@/org/openbravo/distributionorder/erpReports/OBDO_DistOrder.jrxml";
  private static final String DEFAULT_OUTPUT_NAME = "OBDO_DistOrder";
  private static final TemplateData[] EMPTY_ARRAY = {};

  private String templateLocation;
  private String reportFileName;

  /**
   * Creates a new {@code DistributionOrderReportTemplate} with the report template information
   * configured for the document type of the provided distribution order
   *
   * @param distributionOrderId
   *          The ID of a distribution order
   */
  public DistributionOrderReportTemplate(String distributionOrderId) {
    TemplateData[] templates = getDocumentTypeTemplate(distributionOrderId);
    if (templates.length > 0) {
      templateLocation = templates[0].templateLocation + "/" + templates[0].templateFilename;
      reportFileName = templates[0].reportFilename;
    } else {
      templateLocation = DEFAULT_TEMPLATE_FILE;
      reportFileName = DEFAULT_OUTPUT_NAME;
    }
  }

  /**
   * @return a String with the report template location
   */
  public String getTemplateLocation() {
    return templateLocation;
  }

  /**
   * @return a String with the resolved report template location, i.e., replacing the "@basedesign@"
   *         placeholder.
   */
  public String getResolvedTemplateLocation() {
    return templateLocation.replace("@basedesign@", ReportingUtils.getRealBaseDesignPath());
  }

  /**
   * @return a String with the base name of the generated file as result of processing the report
   *         template
   */
  public String getReportFileName() {
    return reportFileName;
  }

  private TemplateData[] getDocumentTypeTemplate(String distributionOrderId) {
    try {
      OBContext.setAdminMode(true);
      DistributionOrder distributionOrder = OBDal.getInstance()
          .get(DistributionOrder.class, distributionOrderId);
      return TemplateData.getDocumentTemplates(
          DalConnectionProvider.getReadOnlyConnectionProvider(),
          distributionOrder.getDocumentType().getId(), distributionOrder.getOrganization().getId());
    } catch (Exception noTemplateFound) {
      log.debug("Could not find report template for distribution order with ID {}",
          distributionOrderId);
      return EMPTY_ARRAY;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

}
