/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017-2022 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.erpReports;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Print_DistOrder extends HttpSecureAppServlet {
  private static final Logger log = LoggerFactory.getLogger(Print_DistOrder.class);
  private static final long serialVersionUID = 1L;

  @Override
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    try {
      VariablesSecureApp vars = new VariablesSecureApp(request);
      if (vars.commandIn("DEFAULT")) {
        String strmDistOrderId = vars.getSessionValue("PRINTDISTRIBUTIONORDER.INPOBDODISTORDERID")
            .replace("('", "")
            .replace("')", "");
        printPagePartePDF(response, vars, strmDistOrderId);
      } else {
        pageError(response);
      }
    } catch (Exception unexpectedException) {
      log.error("Unexpected exception in Print_DistOrder.doPost ", unexpectedException);
    }
  }

  private void printPagePartePDF(HttpServletResponse response, VariablesSecureApp vars,
      String strmDistOrderId) throws ServletException {
    final HashMap<String, Object> parameters = new HashMap<>(1);
    parameters.put("DOCUMENT_ID", strmDistOrderId);
    DistributionOrderReportTemplate template = new DistributionOrderReportTemplate(strmDistOrderId);

    renderJR(vars, response, template.getTemplateLocation(), template.getReportFileName(), "pdf",
        parameters, null, null, false);
  }

  @Override
  public String getServletInfo() {
    return "Servlet that presents the PrintOptions seeker";
  } // End of getServletInfo() method

}
