/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017-2022 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.erpCommon.utility;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.Restrictions;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.weld.WeldUtils;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.DistributionOrderLine;
import org.openbravo.distributionorder.hooks.AfterDistributionOrderProcessedHookRunner;
import org.openbravo.distributionorder.hooks.BeforeDistributionOrderProcessedHookRunner;
import org.openbravo.erpCommon.businessUtility.Preferences;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.PropertyException;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;

public class ProcessDistributionOrderUtil {

  public static final String DOCACTION_CLOSE = "CL";
  public static final String DOCACTION_CONFIRM = "CF";
  public static final String DOCACTION_BOOK = "CO";
  private static final String DOCACTION_NA = "NA";

  public static final String DOCSTATUS_DRAFT = "DR";
  public static final String DOCSTATUS_ONGOING = "OG";
  private static final String DOCSTATUS_BOOKED = "CO";
  private static final String DOCSTATUS_REQUESTED = "RE";
  private static final String DOCSTATUS_CONFIRMED = "CF";
  public static final String DOCSTATUS_CLOSE = "CL";

  private ProcessDistributionOrderUtil() {
    throw new UnsupportedOperationException(
        "This is an utility class, it should never be instantiated");
  }

  /**
   * Processes the given Distribution Order.
   * 
   * <ul>
   * <li>If the Action is Book it also creates the related mirror document (Receipt/Issue) and
   * processes it</li>
   * <li>If the Action is confirm updated the mirror document and creates lines for it if
   * necessary</li>
   * <li>If the Action is Close it updated the document and the mirror one</li>
   * </ul>
   * 
   * @param distOrder
   */
  public static OBError processDistributionOrder(DistributionOrder distOrder) {
    OBContext.setCrossOrgReferenceAdminMode();
    try {
      String docation = distOrder.getDocumentAction();
      OBError msg;

      if (!areValidWarehouses(distOrder)) {
        return getGenericErrorMessage(OBMessageUtils.messageBD("OBDO_DOWarehousesNotValid"));
      }

      runBeforeDistributionOrderProcessedHooks(distOrder, docation);

      if (docation.equals(DOCACTION_BOOK)) {
        DistributionOrder distOrderNew = createDistOrder(distOrder);
        createDistOrderLines(distOrder, distOrderNew);
        distOrder.setReferencedDistorder(distOrderNew);
        setNewStatusAndAction(distOrder);
        saveChangesInDO(distOrder);
        msg = getResultMessage(distOrder, distOrderNew);

      } else if (docation.equals(DOCACTION_CONFIRM)) {
        StringBuilder result = validateQtyConfirm(distOrder);
        if (result.length() > 0) {
          msg = getGenericErrorMessage(result.toString());
        } else {
          createDistOrderLines(distOrder, distOrder.getReferencedDistorder());
          confirmDistOrder(distOrder);
          msg = getGenericSuccessMessage();
        }

      } else { // CLOSE Action
        closeDistributionOrders(distOrder);
        msg = getGenericSuccessMessage();
      }
      runAfterDistributionOrderProcessedHooks(distOrder, docation);
      return msg;
    } finally {
      OBContext.restorePreviousCrossOrgReferenceMode();
    }
  }

  /**
   * Checks if the Warehouses given in the Distribution Order are valid or not. To be valid
   * <ul>
   * <li>Warehouses must be different</li>
   * <li>If Override DO Warehouses Configuration is true, we will not do any validation for
   * Warehouse Issue in Distribution Order Receipt and Warehouse Receipt in Distribution Order
   * Receipt</li>
   * <li>Issue Warehouse must belong to the Legal Entity of the Organization in Distribution Order
   * Issue. It must belong to the selected Organization Tree defined in Default DO organization Tree
   * Scope or to Additional DO Warehouses list defined in the Organization window for Distribution
   * Order Receipt</li>
   * <li>Receipt Warehouse must belong to the Legal Entity of the Organization in Distribution Order
   * Receipt. It must belong to the selected Organization Tree defined in Default DO organization
   * Tree Scope or to Additional DO Warehouses list defined in the Organization window for
   * Distribution Order Issue</li>
   * </ul>
   * 
   * @param distOrder
   *          The Distribution Order that is being processed
   * @return true if the configuration is correct, false otherwise
   */
  private static boolean areValidWarehouses(DistributionOrder distOrder) {
    Organization organization = distOrder.getOrganization();
    Warehouse receiptWarehouse = distOrder.getWarehouseReceipt();
    Warehouse issueWarehouse = distOrder.getWarehouseIssue();

    final boolean areSameWarehouse = StringUtils.equals(receiptWarehouse.getId(),
        issueWarehouse.getId());
    if (areSameWarehouse) {
      return false;
    }

    final boolean isValidWarehouseIssue = isValidWarehouse(issueWarehouse.getId(),
        DistributioOrderUtils.getFilterExpressionForIssue(organization.getId(),
            receiptWarehouse.getId(), distOrder.isSalesTransaction(),
            distOrder.isOverrideDOWarehouseConf()));
    if (!isValidWarehouseIssue) {
      return false;
    }

    // isValidWarehouseReceipt
    return isValidWarehouse(receiptWarehouse.getId(),
        DistributioOrderUtils.getFilterExpressionForReceipt(organization.getId(),
            issueWarehouse.getId(), distOrder.isSalesTransaction(),
            distOrder.isOverrideDOWarehouseConf()));
  }

  private static boolean isValidWarehouse(String warehouseId, String whereClause) {
    if (isOverrideDOWarehouseConfig(whereClause)) {
      return true;
    }

    //@formatter:off
    final String hql = "select e.id "
        + "             from Warehouse e "
        + "             where (" + whereClause + ") "
        + "             and e.id = :warehouseId ";
    //@formatter:on

    return OBDal.getInstance()
        .getSession()
        .createQuery(hql, String.class)
        .setParameter("warehouseId", warehouseId)
        .uniqueResult() != null;
  }

  private static boolean isOverrideDOWarehouseConfig(final String whereClause) {
    return StringUtils.isBlank(whereClause);
  }

  /**
   * Create the copy of the distribution order of the header, assign the type of document depending
   * on whether it is a issue or a receipt
   * 
   * @param distOrder
   *          An object of distribution orders that will contain the information of the stores,
   *          organization and dates
   * 
   */

  private static DistributionOrder createDistOrder(DistributionOrder distOrder) {
    try {
      OBContext.setAdminMode(false); // False required to create a document in another org
      DistributionOrder newDistOrder = OBProvider.getInstance().get(DistributionOrder.class);

      newDistOrder.setClient(distOrder.getClient());
      newDistOrder.setWarehouseIssue(distOrder.getWarehouseIssue());
      newDistOrder.setWarehouseReceipt(distOrder.getWarehouseReceipt());
      if (distOrder.isSalesTransaction()) {
        newDistOrder.setSalesTransaction(false);
        newDistOrder.setDocumentStatus(DOCSTATUS_CONFIRMED);
        newDistOrder.setDocumentAction(DOCACTION_CLOSE);
        newDistOrder.setOrganization(distOrder.getWarehouseReceipt().getOrganization());
      } else {
        newDistOrder.setSalesTransaction(true);
        newDistOrder.setDocumentStatus(DOCSTATUS_REQUESTED);
        newDistOrder.setDocumentAction(DOCACTION_CONFIRM);
        newDistOrder.setOrganization(distOrder.getWarehouseIssue().getOrganization());
      }
      newDistOrder.setDocumentType(distOrder.getDocumentType());
      newDistOrder.setOrderDate(distOrder.getOrderDate());
      newDistOrder.setScheduledDeliveryDate(distOrder.getScheduledDeliveryDate());
      newDistOrder.setReferencedDistorder(distOrder);

      DocumentType doctype = FIN_Utility.getDocumentType(newDistOrder.getOrganization(),
          distOrder.isSalesTransaction() ? "OBDO_DOR" : "OBDO_DOI");
      if (doctype == null) {
        throw new OBException(String.format(OBMessageUtils.messageBD(
            distOrder.isSalesTransaction() ? "OBDO_DocTypeNoFoundDOR" : "OBDO_DocTypeNoFoundDOI"),
            newDistOrder.getOrganization().getName()));
      }
      newDistOrder.setDocumentType(doctype);
      newDistOrder.setDocumentNo(getNewDocumentNo(distOrder));
      newDistOrder.setOverrideDOWarehouseConf(distOrder.isOverrideDOWarehouseConf());
      OBDal.getInstance().save(newDistOrder);
      OBDal.getInstance().flush(); // Flush with admin mode

      return newDistOrder;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Create the distribution order number by obtaining them according to preference
   * 
   * @param distOrder
   *          the original distribution order object from which to obtain the docno to be created in
   *          the new DO object
   * 
   */
  private static String getNewDocumentNo(DistributionOrder distOrder) {
    boolean copyDocNo = getPreferenceValue("OBDO_CopyDocNo");

    return copyDocNo ? distOrder.getDocumentNo()
        : FIN_Utility.getDocumentNo(distOrder.getDocumentType(),
            distOrder.getDocumentType().getTable().getName());
  }

  /**
   * Returns a boolean value of a preference.
   * 
   * @param propertySearchKey
   *          Search Key of the property referenced by the Preference
   * 
   */
  public static boolean getPreferenceValue(String propertySearchKey) {
    try {
      return "Y".equals(Preferences.getPreferenceValue(propertySearchKey, true,
          OBContext.getOBContext().getCurrentClient(),
          OBContext.getOBContext().getCurrentOrganization(), OBContext.getOBContext().getUser(),
          OBContext.getOBContext().getRole(), null));
    } catch (PropertyException e) {
      return false;
    }
  }

  /**
   * Create the distribution order lines by obtaining them from a DO object and adding them to a new
   * DO object
   * 
   * @param distOrder
   *          A distribution order object from which to obtain the lines to be created in the new DO
   *          object
   * @param distOrderNew
   *          A distribution order object in which lines are created
   * 
   */

  private static void createDistOrderLines(DistributionOrder distOrder,
      DistributionOrder distOrderNew) {
    ScrollableResults distorderlineSR = null;

    try {
      OBContext.setAdminMode(false); // False required to create a document in another org
      OBCriteria<DistributionOrderLine> distorderline = OBDal.getInstance()
          .createCriteria(DistributionOrderLine.class);
      distorderline
          .add(Restrictions.eq(DistributionOrderLine.PROPERTY_DISTRIBUTIONORDER, distOrder));
      distorderline
          .add(Restrictions.isNull(DistributionOrderLine.PROPERTY_REFERENCEDDISTORDERLINE));
      distorderline.setFilterOnReadableOrganization(false);
      distorderlineSR = distorderline.scroll(ScrollMode.FORWARD_ONLY);

      int i = 0;
      boolean isSalesTrx = distOrder.isSalesTransaction();
      final Client distOrderClient = OBDal.getInstance()
          .getProxy(Client.class, distOrder.getClient().getId());
      final Organization distOrderOrg = OBDal.getInstance()
          .getProxy(Organization.class,
              isSalesTrx ? distOrder.getWarehouseReceipt().getOrganization().getId()
                  : distOrder.getWarehouseIssue().getOrganization().getId());
      while (distorderlineSR.next()) {

        DistributionOrderLine distOrderline = (DistributionOrderLine) distorderlineSR.get(0);
        DistributionOrderLine distOrderlineNew = OBProvider.getInstance()
            .get(DistributionOrderLine.class);
        distOrderlineNew.setOrganization(distOrderOrg);
        distOrderlineNew.setClient(distOrderClient);
        distOrderlineNew.setDistributionOrder(distOrderNew);
        distOrderlineNew.setProduct(distOrderline.getProduct());
        distOrderlineNew.setUOM(distOrderline.getUOM());
        distOrderlineNew.setLineNo(distOrderline.getLineNo());
        distOrderlineNew.setReferencedDistorderline(distOrderline);
        distOrderlineNew.setOrderedQuantity(distOrderline.getOrderedQuantity());
        if (isSalesTrx) {
          distOrderlineNew.setQtyConfirmed(distOrderline.getQtyConfirmed());
          distOrderlineNew.setOrderedQuantity(BigDecimal.ZERO);
          distOrderline.setOrderedQuantity(BigDecimal.ZERO);
        } else {
          distOrderlineNew.setQtyConfirmed(distOrderline.getOrderedQuantity());
        }
        distOrderlineNew.setQtyIssued(distOrderline.getQtyIssued());
        distOrderlineNew.setQtyReceived(distOrderline.getQtyReceived());
        distOrderlineNew.setOperativeQuantity(distOrderline.getOperativeQuantity());
        distOrderlineNew.setAlternativeUOM(distOrderline.getAlternativeUOM());

        distOrderlineNew.setAttributeSetValue(distOrderline.getAttributeSetValue());

        OBDal.getInstance().save(distOrderlineNew);
        distOrderline.setReferencedDistorderline(distOrderlineNew);
        OBDal.getInstance().save(distOrderline);

        i++;
        if ((i % 100) == 0) {
          OBDal.getInstance().flush(); // Flush with admin mode
          OBDal.getInstance().getSession().clear();
          OBDal.getInstance().refresh(distOrder);
        }
      }

      if (i > 0 && (i % 100) != 0) {
        OBDal.getInstance().flush(); // Flush with admin mode
      }
    } finally {
      if (distorderlineSR != null) {
        distorderlineSR.close();
      }
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Confirms the quantities and updates them in the referenced document, changes the status of the
   * documents
   * 
   * @params distOrder Distribution order object in which you will get the line to update the
   *         QtyConfirmed.
   * 
   */

  private static void confirmDistOrder(DistributionOrder distOrder) {
    ScrollableResults linesSR = null;
    int i = 0;
    try {
      OBContext.setAdminMode(false);

      DistributionOrder distOrderReference = distOrder.getReferencedDistorder();
      distOrderReference.setDocumentStatus(DOCSTATUS_CONFIRMED);
      distOrderReference.setDocumentAction(DOCACTION_CLOSE);
      distOrderReference.setScheduledDeliveryDate(distOrder.getScheduledDeliveryDate());
      OBDal.getInstance().save(distOrderReference);

      distOrder.setDocumentStatus(DOCSTATUS_CONFIRMED);
      distOrder.setDocumentAction(DOCACTION_CLOSE);
      OBDal.getInstance().save(distOrder);

      OBCriteria<DistributionOrderLine> lines = OBDal.getInstance()
          .createCriteria(DistributionOrderLine.class);
      lines.add(
          Restrictions.eq(DistributionOrderLine.PROPERTY_DISTRIBUTIONORDER, distOrderReference));
      linesSR = lines.scroll(ScrollMode.FORWARD_ONLY);

      while (linesSR.next()) {
        DistributionOrderLine line = (DistributionOrderLine) linesSR.get(0);
        line.setQtyConfirmed(line.getReferencedDistorderline().getQtyConfirmed());
        OBDal.getInstance().save(line);

        i++;
        if ((i % 100) == 0) {
          OBDal.getInstance().flush();
          OBDal.getInstance().getSession().clear();
          OBDal.getInstance().refresh(distOrder);
        }
      }

      OBDal.getInstance().flush();
    } finally {
      if (linesSR != null) {
        linesSR.close();
      }
      OBContext.restorePreviousMode();
    }
  }

  /**
   * 
   * Validate that the confirmed amount is not less than zero.
   * 
   * When the confirmed quantity is less than zero an error message returns.
   * 
   * @param distOrder
   *          A distribution order object from which to obtain the lines to be created in the new DO
   *          object.
   * 
   * @return If there are less than zero lines returns an error message, if there are no less than
   *         zero lines returns a null StringBuilder object
   * 
   */

  private static StringBuilder validateQtyConfirm(DistributionOrder distOrder) {
    ScrollableResults linesSR = null;
    DistributionOrder distOrderReference = distOrder.getReferencedDistorder();
    int i = 0;

    try {
      OBCriteria<DistributionOrderLine> lines = OBDal.getInstance()
          .createCriteria(DistributionOrderLine.class);
      lines.add(
          Restrictions.eq(DistributionOrderLine.PROPERTY_DISTRIBUTIONORDER, distOrderReference));

      linesSR = lines.scroll(ScrollMode.FORWARD_ONLY);
      StringBuilder errorMessage = new StringBuilder();
      while (linesSR.next()) {
        DistributionOrderLine line = (DistributionOrderLine) linesSR.get(0);
        if (line.getReferencedDistorderline().getQtyConfirmed().compareTo(BigDecimal.ZERO) < 0) {
          if (errorMessage.length() == 0) {
            errorMessage.append(OBMessageUtils.messageBD("OBDO_QTYCONFIRMED"));
          }
          errorMessage.append(line.getLineNo() + " ");
        }
        i++;
        if ((i % 100) == 0) {
          OBDal.getInstance().flush();
          OBDal.getInstance().getSession().clear();
          OBDal.getInstance().refresh(distOrder);
        }
      }
      return errorMessage;
    } finally {
      if (linesSR != null) {
        linesSR.close();
      }
    }
  }

  /**
   * Close the documents and update the quantities for the quantities received.
   * 
   * Also closes the referenced order if the referenced order is an issue or if it is a receipt with
   * all the issued quantities received.
   * 
   * @params order Object of distribution order that is closing.
   */

  private static void closeDistributionOrders(DistributionOrder order) {
    OBContext.setAdminMode(false); // False required to update a document in another Organization
    try {
      updateConfirmedQuantityForLinesAndReferencedLines(order);
      udpateIssueOrReceiptStatus(order);
      order.setDocumentStatus(DOCSTATUS_CLOSE);
      order.setDocumentAction(DOCACTION_NA);
      OBDal.getInstance().save(order);

      DistributionOrder distOrder = order.getReferencedDistorder();

      // order==receipt: always close the referenced order which is an issue
      // order==issue: only close the receipt (referenced) order if all are received
      boolean referencedIsReceipt = !distOrder.isSalesTransaction();
      boolean closeReferencedOrder = !referencedIsReceipt;
      if (referencedIsReceipt) {

        // flush all the changes to have the latest data in the datbase
        // before the query
        OBDal.getInstance().flush();

        // find any not yet received line
        OBCriteria<DistributionOrderLine> obCriteria = OBDal.getInstance()
            .createCriteria(DistributionOrderLine.class);
        // important to not filter on org as the issueing user might not have access
        // to the receipt order
        obCriteria.setFilterOnReadableOrganization(false);
        Object result = obCriteria
            .add(Restrictions.eq(DistributionOrderLine.PROPERTY_DISTRIBUTIONORDER, distOrder))
            .add(Restrictions.neProperty(DistributionOrderLine.PROPERTY_QTYRECEIVED,
                DistributionOrderLine.PROPERTY_QTYISSUED))
            .setMaxResults(1)
            .uniqueResult();
        // if all have been received then we can close the order
        closeReferencedOrder = result == null;
      }
      if (closeReferencedOrder) {
        udpateIssueOrReceiptStatus(distOrder);
        distOrder.setDocumentStatus(DOCSTATUS_CLOSE);
        distOrder.setDocumentAction(DOCACTION_NA);
        OBDal.getInstance().save(distOrder);
      }
      OBDal.getInstance().flush(); // Flush to persist changes done within setAdminMode(false)
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private static void updateConfirmedQuantityForLinesAndReferencedLines(DistributionOrder order) {
    ScrollableResults distorderlineSR = null;
    try {
      DistributionOrder orderParameter = order;
      if (!order.isSalesTransaction()) {
        orderParameter = order.getReferencedDistorder();
      }

      distorderlineSR = DistributioOrderUtils.getDistOrderLinesScrollableResult(orderParameter);

      int i = 0;
      while (distorderlineSR.next()) {
        DistributionOrderLine distorderline = (DistributionOrderLine) distorderlineSR.get(0);
        distorderline.setQtyConfirmed(distorderline.getReferencedDistorderline().getQtyIssued());
        OBDal.getInstance().save(distorderline);
        DistributionOrderLine distorderlineor = distorderline.getReferencedDistorderline();
        distorderlineor.setQtyConfirmed(distorderlineor.getQtyIssued());
        OBDal.getInstance().save(distorderlineor);

        i++;
        if ((i % 100) == 0) {
          OBDal.getInstance().flush();
          OBDal.getInstance().getSession().clear();
          OBDal.getInstance().refresh(order);
        }
      }

      // Flush always when the lines have been updated to persist the information. It is possible
      // the the status of the Distribution Order might change afterwards, so the changes must be
      // persisted before that happens
      OBDal.getInstance().flush();

    } finally {
      if (distorderlineSR != null) {
        distorderlineSR.close();
      }
    }
  }

  /**
   * Set new Document Status and Document Action to the Distribution Order given as argument
   * 
   * @param distOrder
   *          A Distribution Order object
   */
  private static void setNewStatusAndAction(DistributionOrder distOrder) {
    if (distOrder.isSalesTransaction()) {
      distOrder.setDocumentStatus(DOCSTATUS_CONFIRMED);
      distOrder.setDocumentAction(DOCACTION_CLOSE);
    } else {
      distOrder.setDocumentStatus(DOCSTATUS_BOOKED);
      distOrder.setDocumentAction(DOCACTION_CLOSE);
    }
  }

  /**
   * Save and flush changes done in the Distribution Order given as an argument
   * 
   * @param distOrder
   *          A Distribution Order object
   */
  private static void saveChangesInDO(DistributionOrder distOrder) {
    OBDal.getInstance().save(distOrder);
    OBDal.getInstance().flush();
  }

  /**
   * Returns an OBError object containing the message to be returned to the user
   * 
   * @param distOrder
   *          The original distribution order
   * @param distOrderNew
   *          The distribution order created or related to the original one
   * @return An OBError object
   */
  private static OBError getResultMessage(DistributionOrder distOrder,
      DistributionOrder distOrderNew) {
    OBError msg = new OBError();
    msg.setTitle(OBMessageUtils.messageBD("Success"));
    msg.setMessage(OBMessageUtils.messageBD("Success"));
    msg.setType("Success");
    msg.setMessage(String.format(
        OBMessageUtils.messageBD(
            distOrder.isSalesTransaction() ? "OBDO_DOReceiptCreated" : "OBDO_DOIssueCreated"),
        distOrderNew.getDocumentNo()));
    return msg;
  }

  /**
   * Returns a generic success message in an OBError object
   */
  private static OBError getGenericSuccessMessage() {
    OBError msg = new OBError();
    msg.setTitle(OBMessageUtils.messageBD("Success"));
    msg.setMessage(OBMessageUtils.messageBD("Success"));
    msg.setType("Success");
    return msg;
  }

  /**
   * Returns a generic error message with the given argument in an OBError object
   */
  private static OBError getGenericErrorMessage(String errorMessage) {
    OBError msg = new OBError();
    msg.setMessage(errorMessage);
    msg.setTitle(OBMessageUtils.messageBD("Error"));
    msg.setType("Error");
    return msg;
  }

  /**
   * Updates the Issue or Receipt Status of the given distribution order
   */
  private static void udpateIssueOrReceiptStatus(DistributionOrder order) {
    if (order.isSalesTransaction()) {
      order.setIssueStatus(DistributioOrderUtils.getIssueOrReceiptNewIssueStatusFor(order));
    } else {
      order.setReceiptStatus(DistributioOrderUtils.getIssueOrReceiptNewIssueStatusFor(order));
    }
  }

  private static void runAfterDistributionOrderProcessedHooks(final DistributionOrder distOrder,
      final String docAction) {
    AfterDistributionOrderProcessedHookRunner hooksRunner = WeldUtils
        .getInstanceFromStaticBeanManager(AfterDistributionOrderProcessedHookRunner.class);
    hooksRunner.run(distOrder, docAction);
  }

  private static void runBeforeDistributionOrderProcessedHooks(final DistributionOrder distOrder,
      final String docAction) {
    BeforeDistributionOrderProcessedHookRunner hooksRunner = WeldUtils
        .getInstanceFromStaticBeanManager(BeforeDistributionOrderProcessedHookRunner.class);
    hooksRunner.run(distOrder, docAction);
  }
}
