/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017-2020 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.actionHandler;

import java.util.Map;

import org.codehaus.jettison.json.JSONObject;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.client.application.process.ResponseActionsBuilder.MessageType;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.erpCommon.utility.DistributioOrderUtils;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.service.db.DbUtility;

public class ProcessReceipt extends BaseProcessActionHandler {

  @Override
  protected JSONObject doExecute(final Map<String, Object> parameters, final String content) {
    try {
      final JSONObject jsonData = new JSONObject(content);
      final JSONObject jsonparams = jsonData.getJSONObject("_params");
      final String recordID = jsonData.getString("M_Movement_ID");
      final String locatorId = jsonparams.getString("M_Locator_ID");
      final InternalMovement movement = OBDal.getInstance().get(InternalMovement.class, recordID);

      processReceiveDO(movement, locatorId);

      return getResponseBuilder()
          .showMsgInProcessView(MessageType.SUCCESS, OBMessageUtils.messageBD("Success"),
              OBMessageUtils.messageBD("Success"))
          .build();

    } catch (Exception e) {
      final Throwable sqlException = DbUtility.getUnderlyingSQLException(e);
      return getResponseBuilder()
          .showMsgInProcessView(MessageType.ERROR, OBMessageUtils.messageBD("Error"),
              OBMessageUtils.translateError(sqlException.getMessage()).getMessage())
          .build();
    }
  }

  public static void processReceiveDO(final InternalMovement movement, final String locatorId) {
    if (movement.get(InternalMovement.PROPERTY_OBDOISSUEMOVEMENT) == null) {
      throw new OBException("@OBDO_IssueMovementNotEmptyInReceiveDO@");
    }
    OBContext.setCrossOrgReferenceAdminMode();
    try {
      createMovementLines(movement, locatorId);
      DistributioOrderUtils.processMovement(movement);
      DistributioOrderUtils.updateDistributionOrder(movement);
    } finally {
      OBContext.restorePreviousCrossOrgReferenceMode();
    }

  }

  /**
   * Create and add lines in the goods movement from the selected Issue Distribution order.
   * 
   * @param movement
   *          A goods movement object that will contain the goods movement lines created
   * @param locatorId
   *          Selected location object to assign the location where the products arrive.
   */

  private static void createMovementLines(final InternalMovement movement, final String locatorId) {
    ScrollableResults issueMovLinesSR = null;
    try {

      issueMovLinesSR = OBDal.getInstance()
          .createCriteria(InternalMovementLine.class)
          .add(Restrictions.eq(InternalMovementLine.PROPERTY_MOVEMENT,
              movement.getObdoIssueMovement()))
          .setFilterOnReadableOrganization(false)
          .scroll(ScrollMode.FORWARD_ONLY);

      long lineno = 10;
      int i = 0;

      while (issueMovLinesSR.next()) {
        final InternalMovementLine issueMovline = (InternalMovementLine) issueMovLinesSR.get(0);
        final InternalMovementLine movementline = OBProvider.getInstance()
            .get(InternalMovementLine.class);

        movementline.setOrganization(movement.getOrganization());
        movementline.setMovement(movement);
        movementline.setClient(movement.getClient());
        movementline.setProduct(issueMovline.getProduct());
        movementline.setLineNo(lineno);
        movementline.setMovementQuantity(issueMovline.getMovementQuantity());
        movementline.setAttributeSetValue(issueMovline.getAttributeSetValue());
        movementline.setUOM(issueMovline.getUOM());
        movementline
            .setObdoDistorderline(issueMovline.getObdoDistorderline().getReferencedDistorderline());
        movementline.setStorageBin(issueMovline.getNewStorageBin());

        Locator storageBin = OBDal.getInstance().get(Locator.class, locatorId);
        movementline.setNewStorageBin(storageBin);

        movementline.setOperativeQuantity(issueMovline.getOperativeQuantity());
        movementline.setAlternativeUOM(issueMovline.getAlternativeUOM());

        OBDal.getInstance().save(movementline);
        lineno += 10;
        i++;

        if ((i % 100) == 0) {
          OBDal.getInstance().flush();
          OBDal.getInstance().getSession().clear();
        }
      }
    } catch (Exception e) {
      issueMovLinesSR.close();
    }

  }

}
