/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017-2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.ad_callouts;

import javax.servlet.ServletException;

import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.materialmgmt.UOMUtil;
import org.openbravo.model.common.plm.Product;

public class SL_DistributionOrder_Product extends SimpleCallout {

  @Override
  protected void execute(CalloutInfo info) throws ServletException {
    OBContext.setAdminMode(false);
    try {
      String strProductId = info.getStringParameter("inpmProductId");
      info.addResult("inpcAum", UOMUtil.getDefaultAUMForLogistic(strProductId));
      Product selectedProduct = OBDal.getInstance().get(Product.class, strProductId);
      info.addResult("inpcUomId", selectedProduct.getUOM().getId());
    } finally {
      OBContext.restorePreviousMode();
    }
  }
}
