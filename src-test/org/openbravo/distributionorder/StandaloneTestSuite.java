/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at https://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.distributionorder;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.openbravo.distributionorder.test.OBDOTestSuite;

/** Test suite to be run in CI */
@RunWith(Suite.class)
@Suite.SuiteClasses({ OBDOTestSuite.class })
public class StandaloneTestSuite {
}
