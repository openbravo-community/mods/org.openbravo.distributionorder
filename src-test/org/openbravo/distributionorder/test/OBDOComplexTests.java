/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017-2022 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.test;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.test.utils.DistributionOrderTestUtils;
import org.openbravo.erpCommon.businessUtility.Preferences;
import org.openbravo.model.ad.access.Role;
import org.openbravo.model.ad.domain.Preference;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OBDOComplexTests extends OBDO_FB_WeldBaseTest {

  @Override
  @Before
  public void initialize() {
    log.info("Initializing Distribution Orders Complex Tests...");
    super.initialize();
  }

  /**
   * This test case checks that the basic flow works correctly for Documents with several lines
   * <ul>
   * <li>1. Create a Distribution Order Receipt with 10 lines</li>
   * <li>2. Check that the related Distribution Order Issue has been correctly created</li>
   * <li>3. Create an Issue Distribution Order related to the Distribution Order Issue</li>
   * <li>3. Create an Receive Distribution Order related to the Issue Distribution Order</li>
   * </ul>
   */

  @Test
  public void DOC010_CompleteDistributionOrdersSeveraLines() {
    OBContext.setAdminMode();
    try {
      String docNo = "DOC010";
      List<Product> productList = DistributionOrderTestUtils.getProductList(docNo, 10);

      DistributionOrder distributionOrderReceipt = DistributionOrderTestUtils.createDOReceipt(docNo,
          productList);
      DistributionOrderTestUtils.verifyDOIssueCorrectlyCreated(distributionOrderReceipt, 10);
      DistributionOrder distributionOrderIssue = distributionOrderReceipt.getReferencedDistorder();

      DistributionOrderTestUtils.updateConfirmedQty(distributionOrderIssue, null);

      InternalMovement issueDistributionOrder = DistributionOrderTestUtils.createIssueDO(docNo,
          distributionOrderIssue, 100L);

      DistributionOrderTestUtils.createReceiveDO(docNo, issueDistributionOrder, null, 100L, null);
      DistributionOrderTestUtils
          .assertDistributionOrderDoesNotHaveClosedStatus(distributionOrderReceipt);

    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * This test checks that it is possible to correctly complete de Distribution Order flow with
   * partial quantities in the documents involved
   * <ul>
   * <li>1. Clone a Product to create a new one and create Stock for it</li>
   * <li>2. Create and complete a Distribution Order Receipt of 10 units for that Product</li>
   * <li>3. Check that the related Distribution Order Issue has been created correctly</li>
   * <li>4. Confirm 8 units in the Distribution Order Issue and check that the Distribution Order
   * Receipt has been updated properly</li>
   * <li>5. Create an Issue Distribution Order of 5 Units and complete it. Check the DO Documents
   * are updated</li>
   * <li>6. Create an Issue Distribution Order of 1 Units and complete it. Check the DO Documents
   * are updated</li>
   * <li>7. Create an Issue Distribution Order of 1 Units and complete it. Check the DO Documents
   * are updated</li>
   * <li>8. Close the Distribution Order Issue and check that there is no remaining quantity to be
   * sent</li>
   * <li>9. Create a Receive Distribution Order for 5 units and check that the related DO Documents
   * are upated</li>
   * <li>10. Create a Receive Distribution Order for 1 units and check that the related DO Documents
   * are upated</li>
   * <li>11. Create a Receive Distribution Order for 1 units and check that the related DO Documents
   * are upated</li>
   * </ul>
   */
  @Test
  public void DOC020_CompleteDOFlowWithPartialQuantities() {
    OBContext.setAdminMode();
    try {
      String docNo = "DOC020";
      List<Product> productList = DistributionOrderTestUtils.getProductList(docNo, 1);

      DistributionOrder distributionOrderReceipt = DistributionOrderTestUtils.createDOReceipt(docNo,
          productList);
      DistributionOrderTestUtils.verifyDOIssueCorrectlyCreated(distributionOrderReceipt, 1);

      DistributionOrder distributionOrderIssue = distributionOrderReceipt.getReferencedDistorder();
      DistributionOrderTestUtils.updateConfirmedQty(distributionOrderIssue, new BigDecimal(8));

      InternalMovement issueDistributionOrder1 = DistributionOrderTestUtils
          .createIssueOrReceiptDOAddingLines(docNo, distributionOrderIssue, productList.get(0),
              new BigDecimal(5), new BigDecimal(5), 63L, true);
      InternalMovement issueDistributionOrder2 = DistributionOrderTestUtils
          .createIssueOrReceiptDOAddingLines(docNo, distributionOrderIssue, productList.get(0),
              BigDecimal.ONE, new BigDecimal(6), 75L, true);
      InternalMovement issueDistributionOrder3 = DistributionOrderTestUtils
          .createIssueOrReceiptDOAddingLines(docNo, distributionOrderIssue, productList.get(0),
              BigDecimal.ONE, new BigDecimal(7), 88L, true);

      DistributionOrderTestUtils
          .closeDistributionOrderAndCheckStatusUpdated(distributionOrderIssue);
      DistributionOrderTestUtils.assertThatDistributionOrderHasIssuedStatus(distributionOrderIssue,
          100L);

      DistributionOrderTestUtils.createReceiveDO(docNo, issueDistributionOrder1, null, 71L,
          new BigDecimal(5));
      DistributionOrderTestUtils.createReceiveDO(docNo, issueDistributionOrder2, null, 86L,
          new BigDecimal(6));
      DistributionOrderTestUtils.createReceiveDO(docNo, issueDistributionOrder3, null, 100L,
          new BigDecimal(7));

    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @Test
  public void createDOandVerifyDocNoIsSame() {
    setSameDocNoPrefValue("Y");
    String docNo = "DOC030";
    List<Product> productList = DistributionOrderTestUtils.getProductList(docNo, 1);
    DistributionOrder distributionOrderReceipt = DistributionOrderTestUtils.createDOReceipt(docNo,
        productList);
    DistributionOrder distributionOrderIssue = distributionOrderReceipt.getReferencedDistorder();
    DistributionOrderTestUtils.assertThatDocumentNumbersAreTheSame(distributionOrderIssue,
        distributionOrderReceipt);
    DistributionOrderTestUtils
        .assertDistributionOrderDoesNotHaveClosedStatus(distributionOrderReceipt);
    setSameDocNoPrefValue("N");
  }

  @Test
  public void createDOandVerifyDocNoIsDifferent() {
    setSameDocNoPrefValue("N");
    String docNo = "DOC040";
    List<Product> productList = DistributionOrderTestUtils.getProductList(docNo, 1);
    DistributionOrder distributionOrderReceipt = DistributionOrderTestUtils.createDOReceipt(docNo,
        productList);
    DistributionOrder distributionOrderIssue = distributionOrderReceipt.getReferencedDistorder();
    DistributionOrderTestUtils.assertThatDocumentNumbersAreDifferent(distributionOrderIssue,
        distributionOrderReceipt);
    DistributionOrderTestUtils
        .assertDistributionOrderDoesNotHaveClosedStatus(distributionOrderReceipt);

    setSameDocNoPrefValue("N");
  }

  @Test
  public void createDOandVerifyDocNoIsDifferentIfRoleIsManual() {
    setSameDocNoPrefValue("N");
    String docNo = "DOC050";
    Role userRole = OBContext.getOBContext().getRole();
    userRole.setManual(true);
    OBDal.getInstance().flush();
    List<Product> productList = DistributionOrderTestUtils.getProductList(docNo, 1);
    DistributionOrder distributionOrderReceipt = DistributionOrderTestUtils.createDOReceipt(docNo,
        productList);
    DistributionOrder distributionOrderIssue = distributionOrderReceipt.getReferencedDistorder();
    DistributionOrderTestUtils.assertThatDocumentNumbersAreDifferent(distributionOrderIssue,
        distributionOrderReceipt);
    DistributionOrderTestUtils
        .assertDistributionOrderDoesNotHaveClosedStatus(distributionOrderReceipt);
    userRole.setManual(false);
    OBDal.getInstance().flush();
    setSameDocNoPrefValue("N");
  }

  private static void setSameDocNoPrefValue(final String preferenceValue) {
    final Preference pref = Preferences.setPreferenceValue("OBDO_CopyDocNo", preferenceValue, true,
        OBContext.getOBContext().getCurrentClient(),
        OBContext.getOBContext().getCurrentOrganization(), OBContext.getOBContext().getUser(),
        OBContext.getOBContext().getRole(), null, RequestContext.get().getVariablesSecureApp());
    pref.setClient(OBContext.getOBContext().getCurrentClient());
    OBDal.getInstance().flush();
  }

}
