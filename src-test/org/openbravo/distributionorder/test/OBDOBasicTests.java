/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017-2022 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.test.utils.DistributionOrderHeaderData;
import org.openbravo.distributionorder.test.utils.DistributionOrderLineData;
import org.openbravo.distributionorder.test.utils.DistributionOrderTestUtils;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OBDOBasicTests extends OBDO_FB_WeldBaseTest {

  @Override
  @Before
  public void initialize() {
    log.info("Initializing Distribution Orders Basic Tests...");
    super.initialize();
  }

  /**
   * This test checks that it is possible to correctly create and complete a basic Distribution
   * Order Receipt. It also checks that it is possible to close the document without any Issues
   * <ul>
   * <li>1. Create a Distribution Order Receipt</li>
   * <li>2. Check that the related Distribution Order Issue has been correctly created</li>
   * <li>3. Close the Distribution Order Receipt and verify that the status is correclty
   * updated</li>
   * </ul>
   */

  @Test
  public void DOB010_CreateCompleteCloseDOR() {
    OBContext.setAdminMode();
    try {
      String docNo = "DOB010";
      List<Product> productList = DistributionOrderTestUtils.getProductList(docNo, 1);
      log.info("**product name: " + productList.get(0).getName());
      DistributionOrder distributionOrderReceipt = DistributionOrderTestUtils.createDOReceipt(docNo,
          productList);
      DistributionOrderTestUtils.verifyDOIssueCorrectlyCreated(distributionOrderReceipt, 1);
      DistributionOrderTestUtils
          .closeDistributionOrderAndCheckStatusUpdated(distributionOrderReceipt);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * This test checks that it is possible to correctly create, complete and finally close a
   * Distribution Order Issue
   * <ul>
   * <li>1. Clone a Product to create a new one</li>
   * <li>2. Create and complete a Distribution Order Issue for that Product</li>
   * <li>3. Check that the related Distribution Order Receipt has been created correctly</li>
   * <li>4. Close the Distribution Order Issue and verify that it has been done correctly</li>
   * </ul>
   */
  @Test
  public void DOB020_CompleteAndCloseDOIssue() {
    OBContext.setAdminMode();
    try {
      String docNo = "DOB020";
      List<Product> productList = DistributionOrderTestUtils.getProductList(docNo, 1);

      DistributionOrder distributionOrderIssue = createDOIssueForDOB020(productList.get(0));
      verifyDOReceiptCorrectlyCreatedForDOB020(distributionOrderIssue);
      DistributionOrderTestUtils
          .closeDistributionOrderAndCheckStatusUpdated(distributionOrderIssue);
      DistributionOrderTestUtils.assertThatDistributionOrderHasIssuedStatus(distributionOrderIssue,
          0L);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * This test will check the full Distribution Orders flow without any particular conditions
   * <ul>
   * <li>1. Create a Distribution Order Receipt</li>
   * <li>2. Check that the related Distribution Order Issue has been correctly created</li>
   * <li>3. Create an Issue Distribution Order related to the Distribution Order Issue</li>
   * <li>3. Create an Receive Distribution Order related to the Issue Distribution Order</li>
   * </ul>
   */

  @Test
  public void DOB030_FullBasicFlowDistributionOrders() {
    OBContext.setAdminMode();
    try {
      String docNo = "DOB030";
      List<Product> productList = DistributionOrderTestUtils.getProductList(docNo, 1);

      DistributionOrder distributionOrderReceipt = DistributionOrderTestUtils.createDOReceipt(docNo,
          productList);
      DistributionOrderTestUtils.verifyDOIssueCorrectlyCreated(distributionOrderReceipt, 1);
      DistributionOrder distributionOrderIssue = distributionOrderReceipt.getReferencedDistorder();

      DistributionOrderTestUtils.updateConfirmedQty(distributionOrderIssue, null);

      InternalMovement issueDistributionOrder = DistributionOrderTestUtils
          .createIssueOrReceiptDOAddingLines(docNo, distributionOrderIssue, productList.get(0),
              new BigDecimal(10), new BigDecimal(10), 100L, true);
      DistributionOrderTestUtils.createReceiveDO(docNo, issueDistributionOrder, null, 100L,
          new BigDecimal(10));

    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private DistributionOrder createDOIssueForDOB020(Product product) {
    DistributionOrder distributionOrder = getDistributionOrderReceiptHeaderForDOB020();
    createAndInsertDistributionOrderLinesForDOB020(distributionOrder, product);
    DistributionOrderTestUtils.processDistributionOrderAndVerifyIsProcessed(distributionOrder);
    return distributionOrder;
  }

  private DistributionOrder getDistributionOrderReceiptHeaderForDOB020() {
    String docNo = DistributionOrderTestUtils.getNextDocNoForDistributionOrder("DOB020");
    DocumentType doIssueDocType = DistributionOrderTestUtils.getDistributionOrderIssueDocType();
    DistributionOrderHeaderData distributionOrderHeaderData = new DistributionOrderHeaderData.DistributionOrderHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_ORG_ID)//
        .orderDate(new Date())//
        .scheduledDeliveryDate(new Date())//
        .warehouseReceiptID(DistributionOrderTestUtils.US_WC_WAREHOUSE_ID)//
        .warehouseIssueID(DistributionOrderTestUtils.US_EC_WAREHOUSE_ID)//
        .isSalesTransaction(true)//
        .docTypeID(doIssueDocType.getId())//
        .docNo(docNo)//
        .description("DOB020 Test")//
        .overrideDOWHConfiguration(true)//
        .build();
    return DistributionOrderTestUtils
        .createAndSaveDistributionOrderHeader(distributionOrderHeaderData);
  }

  private void createAndInsertDistributionOrderLinesForDOB020(DistributionOrder distributionOrder,
      Product product) {
    long lineNo = DistributionOrderTestUtils.getNextLineNoForDistributionOrder(distributionOrder);
    DistributionOrderLineData distributionOrderLineData = new DistributionOrderLineData.DistributionOrderLineDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_ORG_ID)//
        .product(product.getId())//
        .uom(DistributionOrderTestUtils.UNIT_UOM_ID)//
        .lineNo(lineNo)//
        .qtyConfirmed(new BigDecimal(10))//
        .build();
    DistributionOrderTestUtils.createAndInsertDistributionOrderLine(distributionOrderLineData,
        distributionOrder);
  }

  private void verifyDOReceiptCorrectlyCreatedForDOB020(DistributionOrder distributionOrder) {
    DistributionOrder distributionOrderReceipt = distributionOrder.getReferencedDistorder();
    // Need to refresh the object in order to retrieve the latest changes done to it
    OBDal.getInstance().refresh(distributionOrderReceipt);
    DistributionOrderTestUtils.assertDistributionOrderIsNotNull(distributionOrderReceipt);
    DistributionOrderTestUtils.assertDistributionOrderCreatedIsRelatedToOriginalDO(
        distributionOrderReceipt, distributionOrder);
    DistributionOrderTestUtils.assertDistributionOrderHasConfirmedStatus(distributionOrderReceipt);
    DistributionOrderTestUtils.assertThatDistributionOrderHasReceiptStatus(distributionOrderReceipt,
        0L);
    DistributionOrderTestUtils.assertDistributionOrderHasNLines(distributionOrderReceipt, 1);
  }

}
