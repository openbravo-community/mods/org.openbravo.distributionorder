/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017-2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.test;

import org.junit.After;
import org.junit.Before;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.weld.test.WeldBaseTest;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.test.utils.DistributionOrderTestUtils;
import org.openbravo.erpCommon.businessUtility.InitialSetupUtility;
import org.openbravo.model.ad.utility.DataSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class OBDO_FB_WeldBaseTest extends WeldBaseTest {
  // Ale Beer Product
  protected static final String ALE_BEER_PRODUCT_ID = "34560A057833457D962F7A573F76F5BB";
  // Be Soft Drinker Inc Business Partner
  protected static final String BE_SOFT_BPARTNER_ID = "FF2EC1D0E28E4F85BE85532ADD556772";

  protected static final Logger log = LoggerFactory.getLogger(OBDO_FB_WeldBaseTest.class);

  @Before
  public void initialize() {
    OBContext.setOBContext(DistributionOrderTestUtils.USER_ID, DistributionOrderTestUtils.ROLE_ID,
        DistributionOrderTestUtils.FB_CLIENT_ID, DistributionOrderTestUtils.US_ORG_ID,
        DistributionOrderTestUtils.LANGUAGE_CODE);

    applyDistributionOrderDataset();

    final OBContext obContext = OBContext.getOBContext();
    final VariablesSecureApp vars = new VariablesSecureApp(obContext.getUser().getId(),
        obContext.getCurrentClient().getId(), obContext.getCurrentOrganization().getId(),
        obContext.getRole().getId(), obContext.getLanguage().getLanguage());
    RequestContext.get().setVariableSecureApp(vars);
    DistributionOrderTestUtils.setDeleteNonConfirmedTaskAndReservationsPreference();

  }

  @After
  public void resume() {
    DistributionOrderTestUtils.unsetDeleteNonConfirmedTaskAndReservationsPreference();
  }

  private void applyDistributionOrderDataset() {
    try {
      InitialSetupUtility.insertReferenceData(
          OBDal.getInstance().get(DataSet.class, "1D7D9873BFCD49FBB9EE18E00DFF32B5"),
          OBContext.getOBContext().getCurrentClient(),
          OBContext.getOBContext().getCurrentOrganization());
    } catch (Exception ignore) {
    }
  }

}
