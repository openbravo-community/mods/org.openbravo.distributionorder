/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.test.utils;

import java.math.BigDecimal;

import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrderLine;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.uom.UOM;

public class IssueOrReceiptDOLineData {
  private Organization organization;
  private long lineNo;
  private Product product;
  private AttributeSetInstance attributeSetInstance;
  private UOM uom;
  private BigDecimal movementQty;
  private Locator storageBinFrom;
  private Locator storageBinTo;
  private DistributionOrderLine relatedDistributionOrderLine;
  private UOM aum;
  private BigDecimal operativeQty;

  private IssueOrReceiptDOLineData(
      IssueOrReceiptDOLineDataBuilder issueOrReceiptDOLineDataBuilder) {
    this.organization = issueOrReceiptDOLineDataBuilder.organization;
    this.lineNo = issueOrReceiptDOLineDataBuilder.lineNo;
    this.product = issueOrReceiptDOLineDataBuilder.product;
    this.attributeSetInstance = issueOrReceiptDOLineDataBuilder.attributeSetInstance;
    this.uom = issueOrReceiptDOLineDataBuilder.uom;
    this.movementQty = issueOrReceiptDOLineDataBuilder.movementQty;
    this.storageBinFrom = issueOrReceiptDOLineDataBuilder.storageBinFrom;
    this.storageBinTo = issueOrReceiptDOLineDataBuilder.storageBinTo;
    this.relatedDistributionOrderLine = issueOrReceiptDOLineDataBuilder.relatedDistributionOrderLine;
    this.aum = issueOrReceiptDOLineDataBuilder.aum;
    this.operativeQty = issueOrReceiptDOLineDataBuilder.operativeQty;
  }

  public Organization getOrganization() {
    return organization;
  }

  public long getLineNo() {
    return lineNo;
  }

  public Product getProduct() {
    return product;
  }

  public AttributeSetInstance getAttributeSetInstance() {
    return attributeSetInstance;
  }

  public UOM getUom() {
    return uom;
  }

  public BigDecimal getMovementQty() {
    return movementQty;
  }

  public Locator getStorageBinFrom() {
    return storageBinFrom;
  }

  public Locator getStorageBinTo() {
    return storageBinTo;
  }

  public DistributionOrderLine getRelatedDistributionOrderLine() {
    return relatedDistributionOrderLine;
  }

  public UOM getAum() {
    return aum;
  }

  public BigDecimal getOperativeQty() {
    return operativeQty;
  }

  public static class IssueOrReceiptDOLineDataBuilder {
    private Organization organization;
    private long lineNo;
    private Product product;
    private AttributeSetInstance attributeSetInstance;
    private UOM uom;
    private BigDecimal movementQty;
    private Locator storageBinFrom;
    private Locator storageBinTo;
    private DistributionOrderLine relatedDistributionOrderLine;
    private UOM aum;
    private BigDecimal operativeQty;

    public IssueOrReceiptDOLineDataBuilder() {

    }

    public IssueOrReceiptDOLineDataBuilder organizationID(String newOrganizationID) {
      Organization newOrganization = OBDal.getInstance().get(Organization.class, newOrganizationID);
      this.organization = newOrganization;
      return this;
    }

    public IssueOrReceiptDOLineDataBuilder lineNo(long newLineNo) {
      this.lineNo = newLineNo;
      return this;
    }

    public IssueOrReceiptDOLineDataBuilder productID(String newProductID) {
      Product newProduct = OBDal.getInstance().get(Product.class, newProductID);
      this.product = newProduct;
      return this;
    }

    public IssueOrReceiptDOLineDataBuilder attributeSetInstanceID(
        String newAttributeSetInstanceID) {
      AttributeSetInstance newAttributeSetInstance = OBDal.getInstance()
          .get(AttributeSetInstance.class, newAttributeSetInstanceID);
      this.attributeSetInstance = newAttributeSetInstance;
      return this;
    }

    public IssueOrReceiptDOLineDataBuilder uomID(String newUOMID) {
      UOM newUOM = OBDal.getInstance().get(UOM.class, newUOMID);
      this.uom = newUOM;
      return this;
    }

    public IssueOrReceiptDOLineDataBuilder movementQty(BigDecimal newMovementQty) {
      this.movementQty = newMovementQty;
      return this;
    }

    public IssueOrReceiptDOLineDataBuilder storageBinFromID(String newStorageBinFromID) {
      Locator newStorageBin = OBDal.getInstance().get(Locator.class, newStorageBinFromID);
      this.storageBinFrom = newStorageBin;
      return this;
    }

    public IssueOrReceiptDOLineDataBuilder storageBinToID(String newStorageBinToID) {
      Locator newStorageBin = OBDal.getInstance().get(Locator.class, newStorageBinToID);
      this.storageBinTo = newStorageBin;
      return this;
    }

    public IssueOrReceiptDOLineDataBuilder distributionOrderLineID(
        String newDistributionOrderLineID) {
      DistributionOrderLine newDistributionOrderLine = OBDal.getInstance()
          .get(DistributionOrderLine.class, newDistributionOrderLineID);
      this.relatedDistributionOrderLine = newDistributionOrderLine;
      return this;
    }

    public IssueOrReceiptDOLineDataBuilder aumID(String newAumID) {
      UOM newAum = OBDal.getInstance().get(UOM.class, newAumID);
      this.aum = newAum;
      return this;
    }

    public IssueOrReceiptDOLineDataBuilder operativeQty(BigDecimal newOperativeQty) {
      this.operativeQty = newOperativeQty;
      return this;
    }

    public IssueOrReceiptDOLineData build() {
      return new IssueOrReceiptDOLineData(this);
    }
  }

}
