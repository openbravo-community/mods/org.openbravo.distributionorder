/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.test.utils;

import java.math.BigDecimal;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.uom.UOM;

public class ShipmentInOutLineData {
  private Organization organization;
  private long lineNo;
  private Product product;
  private BigDecimal movementQty;
  private UOM uom;
  private Locator storageBin;

  private ShipmentInOutLineData(ShipmentInOutLineDataBuilder shipmentInOutLineDataBuilder) {
    this.organization = shipmentInOutLineDataBuilder.organization;
    this.lineNo = shipmentInOutLineDataBuilder.lineNo;
    this.product = shipmentInOutLineDataBuilder.product;
    this.movementQty = shipmentInOutLineDataBuilder.movementQty;
    this.uom = shipmentInOutLineDataBuilder.uom;
    this.storageBin = shipmentInOutLineDataBuilder.storageBin;
  }

  public Organization getOrganization() {
    return organization;
  }

  public long getLineNo() {
    return lineNo;
  }

  public Product getProduct() {
    return product;
  }

  public BigDecimal getMovementQty() {
    return movementQty;
  }

  public UOM getUom() {
    return uom;
  }

  public Locator getStorageBin() {
    return storageBin;
  }

  public static class ShipmentInOutLineDataBuilder {
    private Organization organization;
    private long lineNo;
    private Product product;
    private BigDecimal movementQty;
    private UOM uom;
    private Locator storageBin;

    public ShipmentInOutLineDataBuilder() {

    }

    public ShipmentInOutLineDataBuilder organizationID(String newOrganizationID) {
      Organization newOrganization = OBDal.getInstance().get(Organization.class, newOrganizationID);
      this.organization = newOrganization;
      return this;
    }

    public ShipmentInOutLineDataBuilder lineNo(long newLineNo) {
      this.lineNo = newLineNo;
      return this;
    }

    public ShipmentInOutLineDataBuilder productID(String newProductID) {
      Product newProduct = OBDal.getInstance().get(Product.class, newProductID);
      this.product = newProduct;
      return this;
    }

    public ShipmentInOutLineDataBuilder movementQty(BigDecimal newMovementQty) {
      this.movementQty = newMovementQty;
      return this;
    }

    public ShipmentInOutLineDataBuilder uomID(String newUOMID) {
      UOM newUOM = OBDal.getInstance().get(UOM.class, newUOMID);
      this.uom = newUOM;
      return this;
    }

    public ShipmentInOutLineDataBuilder storageBinID(String newStorageBinID) {
      Locator newStorageBin = OBDal.getInstance().get(Locator.class, newStorageBinID);
      this.storageBin = newStorageBin;
      return this;
    }

    public ShipmentInOutLineData build() {
      return new ShipmentInOutLineData(this);
    }
  }

}
