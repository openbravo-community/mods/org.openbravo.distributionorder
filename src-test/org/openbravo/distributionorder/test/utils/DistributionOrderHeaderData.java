/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017-2022 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.test.utils;

import java.util.Date;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;

public class DistributionOrderHeaderData {
  private Organization organization;
  private DocumentType docType;
  private String docNo;
  private Date orderDate;
  private Date scheduledDeliveryDate;
  private Warehouse warehouseReceipt;
  private Warehouse warehouseIssue;
  private String description;
  private boolean issotrx;
  private boolean overrideDOWHConfiguration;

  private DistributionOrderHeaderData(
      DistributionOrderHeaderDataBuilder distributionOrderHeaderDataBuilder) {
    this.organization = distributionOrderHeaderDataBuilder.organization;
    this.docType = distributionOrderHeaderDataBuilder.docType;
    this.docNo = distributionOrderHeaderDataBuilder.docNo;
    this.orderDate = distributionOrderHeaderDataBuilder.orderDate;
    this.scheduledDeliveryDate = distributionOrderHeaderDataBuilder.scheduledDeliveryDate;
    this.warehouseReceipt = distributionOrderHeaderDataBuilder.warehouseReceipt;
    this.warehouseIssue = distributionOrderHeaderDataBuilder.warehouseIssue;
    this.description = distributionOrderHeaderDataBuilder.description;
    this.issotrx = distributionOrderHeaderDataBuilder.issotrx;
    this.overrideDOWHConfiguration = distributionOrderHeaderDataBuilder.overrideDOWHConfiguration;

  }

  public Organization getOrganization() {
    return organization;
  }

  public DocumentType getDocType() {
    return docType;
  }

  public String getDocNo() {
    return docNo;
  }

  public Date getOrderDate() {
    return orderDate;
  }

  public Date getScheduledDeliveryDate() {
    return scheduledDeliveryDate;
  }

  public Warehouse getWarehouseReceipt() {
    return warehouseReceipt;
  }

  public Warehouse getWarehouseIssue() {
    return warehouseIssue;
  }

  public String getDescription() {
    return description;
  }

  public boolean isSalesTransaction() {
    return issotrx;
  }

  public boolean isOverrideDOWHConfiguration() {
    return overrideDOWHConfiguration;
  }

  public static class DistributionOrderHeaderDataBuilder {
    private Organization organization;
    private DocumentType docType;
    private String docNo;
    private Date orderDate;
    private Date scheduledDeliveryDate;
    private Warehouse warehouseReceipt;
    private Warehouse warehouseIssue;
    private String description;
    private boolean issotrx;
    private boolean overrideDOWHConfiguration;

    public DistributionOrderHeaderDataBuilder() {

    }

    public DistributionOrderHeaderDataBuilder organizationID(String newOrganizationID) {
      Organization newOrganization = OBDal.getInstance().get(Organization.class, newOrganizationID);
      this.organization = newOrganization;
      return this;
    }

    public DistributionOrderHeaderDataBuilder docTypeID(String newDocTypeID) {
      DocumentType newDocType = OBDal.getInstance().get(DocumentType.class, newDocTypeID);
      this.docType = newDocType;
      return this;
    }

    public DistributionOrderHeaderDataBuilder docNo(String newDocNo) {
      this.docNo = newDocNo;
      return this;
    }

    public DistributionOrderHeaderDataBuilder orderDate(Date newOrderDate) {
      this.orderDate = newOrderDate;
      return this;
    }

    public DistributionOrderHeaderDataBuilder scheduledDeliveryDate(Date newScheduledDeliveryDate) {
      this.scheduledDeliveryDate = newScheduledDeliveryDate;
      return this;
    }

    public DistributionOrderHeaderDataBuilder warehouseReceiptID(String newWarehouseReceiptID) {
      Warehouse newWarehouseReceipt = OBDal.getInstance()
          .get(Warehouse.class, newWarehouseReceiptID);
      this.warehouseReceipt = newWarehouseReceipt;
      return this;
    }

    public DistributionOrderHeaderDataBuilder warehouseIssueID(String newWarehouseIssueID) {
      Warehouse newWarehouseIssue = OBDal.getInstance().get(Warehouse.class, newWarehouseIssueID);
      this.warehouseIssue = newWarehouseIssue;
      return this;
    }

    public DistributionOrderHeaderDataBuilder description(String newDescription) {
      this.description = newDescription;
      return this;
    }

    public DistributionOrderHeaderDataBuilder isSalesTransaction(boolean newIsSalesTransaction) {
      this.issotrx = newIsSalesTransaction;
      return this;
    }

    public DistributionOrderHeaderDataBuilder overrideDOWHConfiguration(
        boolean newOverrideDOWHConfiguration) {
      this.overrideDOWHConfiguration = newOverrideDOWHConfiguration;
      return this;
    }

    public DistributionOrderHeaderData build() {
      return new DistributionOrderHeaderData(this);
    }

  }

}
