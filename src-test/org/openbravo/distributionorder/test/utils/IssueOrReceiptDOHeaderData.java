/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2017 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.test.utils;

import java.util.Date;

import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.DistributionOrderV;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.enterprise.Organization;

public class IssueOrReceiptDOHeaderData {
  private Organization organization;
  private String name;
  private String description;
  private Date movementDate;
  private DistributionOrder relatedDistributionOrder;
  private Locator inTransitBin;
  private DistributionOrderV relatedIssueDOView;
  private boolean issotrx;

  private IssueOrReceiptDOHeaderData(
      IssueOrReceiptDOHeaderDataBuilder issueOrReceiptDOHeaderDataBuilder) {
    this.organization = issueOrReceiptDOHeaderDataBuilder.organization;
    this.name = issueOrReceiptDOHeaderDataBuilder.name;
    this.description = issueOrReceiptDOHeaderDataBuilder.description;
    this.movementDate = issueOrReceiptDOHeaderDataBuilder.movementDate;
    this.relatedDistributionOrder = issueOrReceiptDOHeaderDataBuilder.relatedDistributionOrder;
    this.inTransitBin = issueOrReceiptDOHeaderDataBuilder.inTransitBin;
    this.relatedIssueDOView = issueOrReceiptDOHeaderDataBuilder.relatedIssueDOView;
    this.issotrx = issueOrReceiptDOHeaderDataBuilder.issotrx;
  }

  public Organization getOrganization() {
    return organization;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public Date getMovementDate() {
    return movementDate;
  }

  public DistributionOrder getRelatedDistributionOrder() {
    return relatedDistributionOrder;
  }

  public Locator getInTransitBin() {
    return inTransitBin;
  }

  public DistributionOrderV getRelatedIssueDO() {
    return relatedIssueDOView;
  }

  public boolean isSalesTransaction() {
    return issotrx;
  }

  public static class IssueOrReceiptDOHeaderDataBuilder {
    private Organization organization;
    private String name;
    private String description;
    private Date movementDate;
    private DistributionOrder relatedDistributionOrder;
    private Locator inTransitBin;
    private DistributionOrderV relatedIssueDOView;
    private boolean issotrx;

    public IssueOrReceiptDOHeaderDataBuilder() {

    }

    public IssueOrReceiptDOHeaderDataBuilder organizationID(String newOrganizationID) {
      Organization newOrganization = OBDal.getInstance().get(Organization.class, newOrganizationID);
      this.organization = newOrganization;
      return this;
    }

    public IssueOrReceiptDOHeaderDataBuilder name(String newName) {
      this.name = newName;
      return this;
    }

    public IssueOrReceiptDOHeaderDataBuilder description(String newDescription) {
      this.description = newDescription;
      return this;
    }

    public IssueOrReceiptDOHeaderDataBuilder movementDate(Date newMovementDate) {
      this.movementDate = newMovementDate;
      return this;
    }

    public IssueOrReceiptDOHeaderDataBuilder distributionOrderID(String newDistributionOrderID) {
      DistributionOrder newDistributionOrder = OBDal.getInstance()
          .get(DistributionOrder.class, newDistributionOrderID);
      this.relatedDistributionOrder = newDistributionOrder;
      return this;
    }

    public IssueOrReceiptDOHeaderDataBuilder inTransitBinID(String newStorageBinID) {
      Locator newStorageBin = OBDal.getInstance().get(Locator.class, newStorageBinID);
      this.inTransitBin = newStorageBin;
      return this;
    }

    public IssueOrReceiptDOHeaderDataBuilder issueDOViewID(String newIssueDOViewID) {
      DistributionOrderV newIssueDOView = OBDal.getInstance()
          .get(DistributionOrderV.class, newIssueDOViewID);
      this.relatedIssueDOView = newIssueDOView;
      return this;
    }

    public IssueOrReceiptDOHeaderDataBuilder isSalesTransaction(boolean newIsSalesTransaction) {
      this.issotrx = newIsSalesTransaction;
      return this;
    }

    public IssueOrReceiptDOHeaderData build() {
      return new IssueOrReceiptDOHeaderData(this);
    }

  }

}
