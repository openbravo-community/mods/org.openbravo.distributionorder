/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2022 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.Date;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.distributionorder.AdditionalDOWarehouses;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.erpCommon.utility.ProcessDistributionOrderUtil;
import org.openbravo.distributionorder.test.utils.DistributionOrderHeaderData;
import org.openbravo.distributionorder.test.utils.DistributionOrderTestUtils;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OBDOWHVisibilityTests extends OBDO_FB_WeldBaseTest {

  /**
   * This test checks the Warehouse visibility for an Organization based on "Default DO organization
   * tree scope" selector for a Ditribution Order Issue
   * <ul>
   * <li>1. Create a new Distribution Order Issue</li>
   * <li>2. Set to null "Default DO organization tree scope" and check that the DOi cannot be
   * processed with default West Coast WH as WH Receipt</li>
   * <li>3. Set to US Org "Default DO organization tree scope" and check that the DOi cannot be
   * processed with España Region Norte as WH Receipt</li>
   * <li>4. Set WH Receipt to West Coast WH and check that we can process the Distribution Order
   * Issue</li>
   * </ul>
   */
  @Test
  public void DOWH010_CheckWHReceiptInDOIssueDefaultDOOrgTree() {
    OBContext.setAdminMode();
    try {
      Organization usOrganization = OBDal.getInstance()
          .get(Organization.class, DistributionOrderTestUtils.US_ORG_ID);
      DistributionOrder distributionOrderIssue = getDistributionOrderHeaderForDOWH010(true);
      usOrganization.setObdoAdOrg(null);
      OBError processResultWithNoDOOrg = ProcessDistributionOrderUtil
          .processDistributionOrder(distributionOrderIssue);
      assertThatDistributionOrderProcessResult(processResultWithNoDOOrg, "Error");
      usOrganization.setObdoAdOrg(usOrganization);
      distributionOrderIssue.setWarehouseReceipt(
          OBDal.getInstance().get(Warehouse.class, DistributionOrderTestUtils.ES_RN_WAREHOUSE_ID));
      OBError processResultWithWHNotIncluded = ProcessDistributionOrderUtil
          .processDistributionOrder(distributionOrderIssue);
      assertThatDistributionOrderProcessResult(processResultWithWHNotIncluded, "Error");
      distributionOrderIssue.setWarehouseReceipt(
          OBDal.getInstance().get(Warehouse.class, DistributionOrderTestUtils.US_WC_WAREHOUSE_ID));
      ProcessDistributionOrderUtil.processDistributionOrder(distributionOrderIssue);
      DistributionOrderTestUtils.assertDistributionOrderProcessed(distributionOrderIssue);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * This test checks the Warehouse visibility for an Organization based on "Additional DO
   * Warehouses" Tab for a Ditribution Order Issue
   * <ul>
   * <li>1. Create a new Distribution Order Issue</li>
   * <li>2. Set to null "Default DO organization tree scope", create an Additional DO WH with "WH
   * Receipt for DOi" to false and check that the DOi cannot be processed</li>
   * <li>3. Set the Additional DO WH with "WH Receipt for DOi" to true and check that the DOi is
   * processed</li>
   * </ul>
   */
  @Test
  public void DOWH010_CheckWHReceiptInDOIssueAdditionalWH() {
    OBContext.setAdminMode();
    try {
      Organization usOrganization = OBDal.getInstance()
          .get(Organization.class, DistributionOrderTestUtils.US_ORG_ID);

      DistributionOrder distributionOrderIssue = getDistributionOrderHeaderForDOWH010(true);
      Warehouse wcWarehouse = OBDal.getInstance()
          .get(Warehouse.class, DistributionOrderTestUtils.US_WC_WAREHOUSE_ID);
      distributionOrderIssue.setWarehouseReceipt(wcWarehouse);
      usOrganization.setObdoAdOrg(null);
      AdditionalDOWarehouses additionalWH = createAdditionalDOWarehouse(usOrganization,
          wcWarehouse);
      additionalWH.setAvailableWarehouseReceiptForDOi(false);
      OBError processResultWithNoDOOrg = ProcessDistributionOrderUtil
          .processDistributionOrder(distributionOrderIssue);
      assertThatDistributionOrderProcessResult(processResultWithNoDOOrg, "Error");
      additionalWH.setAvailableWarehouseReceiptForDOi(true);
      OBDal.getInstance().flush(); // Required for the DB to see the change
      ProcessDistributionOrderUtil.processDistributionOrder(distributionOrderIssue);
      DistributionOrderTestUtils.assertDistributionOrderProcessed(distributionOrderIssue);
      usOrganization.getOBDOAdditionalDOWarehousesList().remove(additionalWH);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * This test checks the Warehouse visibility for an Organization based on “Override DO Warehouses
   * configuration” checkbox for a Ditribution Order Issue
   * <ul>
   * <li>1. Create a new Distribution Order Issue</li>
   * <li>2. Set to null "Default DO organization tree scope" and check that the DOi cannot be
   * processed with default West Coast WH as WH Receipt</li>
   * <li>3. Set to true “Override DO Warehouses configuration” and check that the DOi is
   * processed</li>
   * </ul>
   */
  @Test
  public void DOWH010_CheckWHReceiptInDOIssueOverrideDOWHConf() {
    OBContext.setAdminMode();
    try {
      Organization usOrganization = OBDal.getInstance()
          .get(Organization.class, DistributionOrderTestUtils.US_ORG_ID);
      DistributionOrder distributionOrderIssue = getDistributionOrderHeaderForDOWH010(true);
      usOrganization.setObdoAdOrg(null);
      OBError processResultWithNoDOOrg = ProcessDistributionOrderUtil
          .processDistributionOrder(distributionOrderIssue);
      assertThatDistributionOrderProcessResult(processResultWithNoDOOrg, "Error");
      distributionOrderIssue.setOverrideDOWarehouseConf(true);
      distributionOrderIssue.setWarehouseReceipt(
          OBDal.getInstance().get(Warehouse.class, DistributionOrderTestUtils.US_WC_WAREHOUSE_ID));
      ProcessDistributionOrderUtil.processDistributionOrder(distributionOrderIssue);
      DistributionOrderTestUtils.assertDistributionOrderProcessed(distributionOrderIssue);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * This test checks the Warehouse visibility for an Organization based on "Default DO organization
   * tree scope" selector for a Ditribution Order Receipt
   * <ul>
   * <li>1. Create a new Distribution Order Receipt</li>
   * <li>2. Set to null "Default DO organization tree scope" and check that the DOr cannot be
   * processed with default West Coast WH as WH Issue</li>
   * <li>3. Set to US Org "Default DO organization tree scope" and check that the DOr cannot be
   * processed with España Region Norte as WH Issue</li>
   * <li>4. Set WH Issue to West Coast WH and check that we can process the Distribution Order
   * Receipt</li>
   * </ul>
   */
  @Test
  public void DOWH010_CheckWHIssueInDOReceiptDefaultDOOrgTree() {
    OBContext.setAdminMode();
    try {
      Organization usOrganization = OBDal.getInstance()
          .get(Organization.class, DistributionOrderTestUtils.US_ORG_ID);
      DistributionOrder distributionOrderReceipt = getDistributionOrderHeaderForDOWH010(false);
      usOrganization.setObdoAdOrg(null);
      OBError processResultWithNoDOOrg = ProcessDistributionOrderUtil
          .processDistributionOrder(distributionOrderReceipt);
      assertThatDistributionOrderProcessResult(processResultWithNoDOOrg, "Error");
      usOrganization.setObdoAdOrg(usOrganization);
      distributionOrderReceipt.setWarehouseIssue(
          OBDal.getInstance().get(Warehouse.class, DistributionOrderTestUtils.ES_RN_WAREHOUSE_ID));
      OBError processResultWithWHNotIncluded = ProcessDistributionOrderUtil
          .processDistributionOrder(distributionOrderReceipt);
      assertThatDistributionOrderProcessResult(processResultWithWHNotIncluded, "Error");
      distributionOrderReceipt.setWarehouseIssue(
          OBDal.getInstance().get(Warehouse.class, DistributionOrderTestUtils.US_EC_WAREHOUSE_ID));
      ProcessDistributionOrderUtil.processDistributionOrder(distributionOrderReceipt);
      DistributionOrderTestUtils.assertDistributionOrderProcessed(distributionOrderReceipt);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * This test checks the Warehouse visibility for an Organization based on "Additional DO
   * Warehouses" Tab for a Ditribution Order Receipt
   * <ul>
   * <li>1. Create a new Distribution Order Receipt</li>
   * <li>2. Set to null "Default DO organization tree scope", create an Additional DO WH with "WH
   * Issue for DOr" to false and check that the DOr cannot be processed</li>
   * <li>3. Set the Additional DO WH with "WH Issue for DOr" to true and check that the DOr is
   * processed</li>
   * </ul>
   */
  @Test
  public void DOWH010_CheckWHIssueInDOReceiptAdditionalWH() {
    OBContext.setAdminMode();
    try {
      Organization usOrganization = OBDal.getInstance()
          .get(Organization.class, DistributionOrderTestUtils.US_ORG_ID);

      DistributionOrder distributionOrderReceipt = getDistributionOrderHeaderForDOWH010(false);
      Warehouse ecWarehouse = OBDal.getInstance()
          .get(Warehouse.class, DistributionOrderTestUtils.US_EC_WAREHOUSE_ID);
      distributionOrderReceipt.setWarehouseIssue(ecWarehouse);
      usOrganization.setObdoAdOrg(null);
      AdditionalDOWarehouses additionalWH = createAdditionalDOWarehouse(usOrganization,
          ecWarehouse);
      additionalWH.setAvailableWarehouseIssueForDOr(false);
      OBError processResultWithNoDOOrg = ProcessDistributionOrderUtil
          .processDistributionOrder(distributionOrderReceipt);
      assertThatDistributionOrderProcessResult(processResultWithNoDOOrg, "Error");
      additionalWH.setAvailableWarehouseIssueForDOr(true);
      OBDal.getInstance().flush(); // Required for the DB to see the change
      ProcessDistributionOrderUtil.processDistributionOrder(distributionOrderReceipt);
      DistributionOrderTestUtils.assertDistributionOrderProcessed(distributionOrderReceipt);
      usOrganization.getOBDOAdditionalDOWarehousesList().remove(additionalWH);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * This test checks the Warehouse visibility for an Organization based on “Override DO Warehouses
   * configuration” checkbox for a Ditribution Order Receipt
   * <ul>
   * <li>1. Create a new Distribution Order Receipt</li>
   * <li>2. Set to null "Default DO organization tree scope" and check that the DOr cannot be
   * processed with default West Coast WH as WH Issue</li>
   * <li>3. Set to true “Override DO Warehouses configuration” and check that the DOr is
   * processed</li>
   * </ul>
   */
  @Test
  public void DOWH010_CheckWHIssueInDOReceiptOverrideDOWHConf() {
    OBContext.setAdminMode();
    try {
      Organization usOrganization = OBDal.getInstance()
          .get(Organization.class, DistributionOrderTestUtils.US_ORG_ID);
      DistributionOrder distributionOrderReceipt = getDistributionOrderHeaderForDOWH010(false);
      usOrganization.setObdoAdOrg(null);
      OBError processResultWithNoDOOrg = ProcessDistributionOrderUtil
          .processDistributionOrder(distributionOrderReceipt);
      assertThatDistributionOrderProcessResult(processResultWithNoDOOrg, "Error");
      distributionOrderReceipt.setOverrideDOWarehouseConf(true);
      distributionOrderReceipt.setWarehouseIssue(
          OBDal.getInstance().get(Warehouse.class, DistributionOrderTestUtils.US_EC_WAREHOUSE_ID));
      ProcessDistributionOrderUtil.processDistributionOrder(distributionOrderReceipt);
      DistributionOrderTestUtils.assertDistributionOrderProcessed(distributionOrderReceipt);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private DistributionOrder getDistributionOrderHeaderForDOWH010(boolean isIssue) {
    String docNo = DistributionOrderTestUtils.getNextDocNoForDistributionOrder("DOWH010");
    DocumentType doIssueDocType = isIssue
        ? DistributionOrderTestUtils.getDistributionOrderIssueDocType()
        : DistributionOrderTestUtils.getDistributionOrderReceiptDocType();
    DistributionOrderHeaderData distributionOrderHeaderData = new DistributionOrderHeaderData.DistributionOrderHeaderDataBuilder()//
        .organizationID(DistributionOrderTestUtils.US_ORG_ID)//
        .orderDate(new Date())//
        .scheduledDeliveryDate(new Date())//
        .warehouseReceiptID(DistributionOrderTestUtils.US_WC_WAREHOUSE_ID)//
        .warehouseIssueID(DistributionOrderTestUtils.US_EC_WAREHOUSE_ID)//
        .isSalesTransaction(isIssue)//
        .docTypeID(doIssueDocType.getId())//
        .docNo(docNo)//
        .description("DOWH010 Test")//
        .build();
    return DistributionOrderTestUtils
        .createAndSaveDistributionOrderHeader(distributionOrderHeaderData);
  }

  private static void assertThatDistributionOrderProcessResult(OBError result,
      String expectedResult) {
    assertThat("The Distribution Order process has finished with " + result.getType(),
        result.getType(), equalTo(expectedResult));
  }

  private AdditionalDOWarehouses createAdditionalDOWarehouse(Organization organization,
      Warehouse warehouse) {
    AdditionalDOWarehouses additionalWH = OBProvider.getInstance()
        .get(AdditionalDOWarehouses.class);
    additionalWH.setOrganization(organization);
    additionalWH.setWarehouse(warehouse);
    List<AdditionalDOWarehouses> additionalWHList = organization
        .getOBDOAdditionalDOWarehousesList();
    additionalWHList.add(additionalWH);
    return additionalWH;
  }

}
