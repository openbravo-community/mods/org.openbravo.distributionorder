/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2022 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.distributionorder.test;

import java.math.BigDecimal;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.distributionorder.DistributionOrder;
import org.openbravo.distributionorder.test.utils.DistributionOrderTestUtils;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OBDOAutomaticCloseTests extends OBDO_FB_WeldBaseTest {

  @Override
  @Before
  public void initialize() {
    log.info("Initializing Distribution Orders Automatic Close Tests...");
    super.initialize();
  }

  @Override
  @After
  public void resume() {
    log.info("Resuming Distribution Orders Automatic Close Tests...");
    DistributionOrderTestUtils.removePreference("OBDO_CloseAfterReceipt");
    super.resume();
  }

  /**
   * This test case checks that the automatic close is working when the preference is set
   * <ul>
   * <li>1. Create a Distribution Order Receipt with 1 lines</li>
   * <li>2. Set the preference 'Close DO Receipt automatically after fully reception' to Y</li>
   * <li>3. Check that the related Distribution Order Issue has been correctly created</li>
   * <li>4. Create an Issue Distribution Order related to the Distribution Order Issue</li>
   * <li>5. Create an Receive Distribution Order related to the Issue Distribution Order</li>
   * <li>6. Check that the Distribution Order Receipt has been closed</li>
   * </ul>
   */

  @Test
  public void DOC010_CheckAutomaticCloseDistributionOrder() {
    OBContext.setAdminMode();
    try {
      String docNo = "DOC010";
      List<Product> productList = DistributionOrderTestUtils.getProductList(docNo, 1);
      DistributionOrderTestUtils.setPrefValue("OBDO_CloseAfterReceipt", "Y");
      DistributionOrder distributionOrderReceipt = DistributionOrderTestUtils.createDOReceipt(docNo,
          productList);
      DistributionOrderTestUtils.verifyDOIssueCorrectlyCreated(distributionOrderReceipt, 1);
      DistributionOrder distributionOrderIssue = distributionOrderReceipt.getReferencedDistorder();

      DistributionOrderTestUtils.updateConfirmedQty(distributionOrderIssue, null);

      InternalMovement issueDistributionOrder = DistributionOrderTestUtils.createIssueDO(docNo,
          distributionOrderIssue, 100L);

      DistributionOrderTestUtils.createReceiveDO(docNo, issueDistributionOrder,
          distributionOrderReceipt, 100L, null);
      DistributionOrderTestUtils.assertDistributionOrderHasClosedStatus(distributionOrderReceipt);

    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * This test case checks that the automatic close is not performed if the preference is not set to
   * Y
   * <ul>
   * <li>1. Create a Distribution Order Receipt with 1 lines</li>
   * <li>2. Check that the related Distribution Order Issue has been correctly created</li>
   * <li>3. Create an Issue Distribution Order related to the Distribution Order Issue</li>
   * <li>4. Create an Receive Distribution Order related to the Issue Distribution Order</li>
   * <li>5. Check that the Distribution Order Receipt has not been closed</li>
   * </ul>
   */

  @Test
  public void DOC020_CheckNotAutomaticCloseDistributionOrder() {
    OBContext.setAdminMode();
    try {
      String docNo = "DOC020";
      List<Product> productList = DistributionOrderTestUtils.getProductList(docNo, 1);
      DistributionOrder distributionOrderReceipt = DistributionOrderTestUtils.createDOReceipt(docNo,
          productList);
      DistributionOrderTestUtils.verifyDOIssueCorrectlyCreated(distributionOrderReceipt, 1);
      DistributionOrder distributionOrderIssue = distributionOrderReceipt.getReferencedDistorder();

      DistributionOrderTestUtils.updateConfirmedQty(distributionOrderIssue, null);

      InternalMovement issueDistributionOrder = DistributionOrderTestUtils.createIssueDO(docNo,
          distributionOrderIssue, 100L);

      DistributionOrderTestUtils.createReceiveDO(docNo, issueDistributionOrder,
          distributionOrderReceipt, 100L, null);
      DistributionOrderTestUtils
          .assertDistributionOrderDoesNotHaveClosedStatus(distributionOrderReceipt);

    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * This test case checks that the process is not closing automatically the DO receipt because the
   * quantity received is less than the quantity confirmed
   * <ul>
   * <li>1. Create a Distribution Order Receipt with 1 lines</li>
   * <li>2. Set the preference 'Close DO Receipt automatically after fully reception' to Y</li>
   * <li>3. Check that the related Distribution Order Issue has been correctly created</li>
   * <li>4. Create an Issue Distribution Order related to the Distribution Order Issue</li>
   * <li>5. Create an Receive Distribution Order related to the Issue Distribution Order but with
   * partial reception</li>
   * <li>6. Check that the Distribution Order Receipt is not closed</li>
   * <li>7. Set the preference 'Close DO Receipt automatically after fully reception' to N</li>
   * </ul>
   */

  @Test
  public void DOC030_CheckNotClosedWhenNotQuantityConfirmedDistributionOrder() {
    OBContext.setAdminMode();
    try {
      String docNo = "DOC030";
      List<Product> productList = DistributionOrderTestUtils.getProductList(docNo, 1);
      DistributionOrderTestUtils.setPrefValue("OBDO_CloseAfterReceipt", "Y");

      DistributionOrder distributionOrderReceipt = DistributionOrderTestUtils.createDOReceipt(docNo,
          productList);
      DistributionOrderTestUtils.verifyDOIssueCorrectlyCreated(distributionOrderReceipt, 1);
      DistributionOrder distributionOrderIssue = distributionOrderReceipt.getReferencedDistorder();

      DistributionOrderTestUtils.updateConfirmedQty(distributionOrderIssue, null);

      DistributionOrderTestUtils.createIssueOrReceiptDOAddingLines(docNo, distributionOrderIssue,
          productList.get(0), new BigDecimal(10), new BigDecimal(10), 100L, true);
      DistributionOrderTestUtils.createIssueOrReceiptDOAddingLines(docNo, distributionOrderReceipt,
          productList.get(0), new BigDecimal(5), new BigDecimal(5), 50L, false);
      DistributionOrderTestUtils
          .assertDistributionOrderDoesNotHaveClosedStatus(distributionOrderReceipt);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * This test case checks that the process is not closing automatically the DO receipt because the
   * quantity received equal to the quantity issued but the issue distribution order is not closed
   * <ul>
   * <li>1. Create a Distribution Order Receipt with 1 lines</li>
   * <li>2. Set the preference 'Close DO Receipt automatically after fully reception' to Y</li>
   * <li>3. Check that the related Distribution Order Issue has been correctly created</li>
   * <li>4. Create an Issue Distribution Order related to the Distribution Order Issue with partial
   * issued qty</li>
   * <li>5. Create an Receive Distribution Order related to the Issue Distribution Order but with
   * partial reception, less than the confirmed qty and equal to the issued qty</li>
   * <li>6. Check that the Distribution Order Receipt is not closed</li>
   * <li>7. Set the preference 'Close DO Receipt automatically after fully reception' to N</li>
   * </ul>
   */

  @Test
  public void DOC040_CheckNotClosedWhenIssuedIsNotClosedDistributionOrder() {
    OBContext.setAdminMode();
    try {
      String docNo = "DOC040";
      List<Product> productList = DistributionOrderTestUtils.getProductList(docNo, 1);
      DistributionOrderTestUtils.setPrefValue("OBDO_CloseAfterReceipt", "Y");

      DistributionOrder distributionOrderReceipt = DistributionOrderTestUtils.createDOReceipt(docNo,
          productList);
      DistributionOrderTestUtils.verifyDOIssueCorrectlyCreated(distributionOrderReceipt, 1);
      DistributionOrder distributionOrderIssue = distributionOrderReceipt.getReferencedDistorder();

      DistributionOrderTestUtils.updateConfirmedQty(distributionOrderIssue, null);

      DistributionOrderTestUtils.createIssueOrReceiptDOAddingLines(docNo, distributionOrderIssue,
          productList.get(0), new BigDecimal(5), new BigDecimal(5), 50L, true);
      DistributionOrderTestUtils.createIssueOrReceiptDOAddingLines(docNo, distributionOrderReceipt,
          productList.get(0), new BigDecimal(5), new BigDecimal(5), 50L, false);
      DistributionOrderTestUtils
          .assertDistributionOrderDoesNotHaveClosedStatus(distributionOrderReceipt);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }
}
